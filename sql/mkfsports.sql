CREATE TABLE admin_log (
  id INT(11) NOT NULL AUTO_INCREMENT,
  titolo VARCHAR(50) DEFAULT NULL,
  testo VARCHAR(255) DEFAULT NULL,
  table_ref VARCHAR(100) DEFAULT NULL,
  op_type VARCHAR(10) NOT NULL,
  insert_at datetime NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE admin_user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50) DEFAULT NULL,
  cognome VARCHAR(50) DEFAULT NULL,
  email VARCHAR(50) NOT NULL,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  ultimo_accesso datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (username)
);

-- uomo / donna / bambino
CREATE TABLE clientela (
    id INT(11) NOT NULL AUTO_INCREMENT,
    titolo VARCHAR(40) NOT NULL UNIQUE,
    PRIMARY KEY(id),
    UNIQUE (titolo)
);
INSERT INTO clientela(titolo) VALUES ('DONNA');
INSERT INTO clientela(titolo) VALUES ('UOMO');

-- maglia / pantalone / ...
CREATE TABLE categoria (
    id int(11) NOT NULL AUTO_INCREMENT,
    titolo varchar(40) NOT NULL,
    clientela_id int(11) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (titolo,clientela_id),
    KEY clientela_id (clientela_id)
);

CREATE TABLE colore (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(40) NOT NULL UNIQUE,
    codice VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE (nome)
);

CREATE TABLE collezione (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL UNIQUE,
    PRIMARY KEY(id),
    UNIQUE (nome)
);

CREATE TABLE prodotto_generico (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL UNIQUE,
    descrizione TEXT NOT NULL,
    attivo BOOLEAN NOT NULL,
    categoria_id INT(11) NOT NULL,
    collezione_id INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(categoria_id) REFERENCES categoria(id),
    FOREIGN KEY(collezione_id) REFERENCES collezione(id),
    UNIQUE (nome)
);

CREATE TABLE prodotto (
    id INT(11) NOT NULL AUTO_INCREMENT,
    attivo BOOLEAN NOT NULL,
    colore_id INT(11) NOT NULL,
    prodotto_generico_id INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(colore_id) REFERENCES colore(id),
    FOREIGN KEY(prodotto_generico_id) REFERENCES prodotto_generico(id)
);

CREATE TABLE prodotto_correlato (
    id INT(11) NOT NULL AUTO_INCREMENT,
    prodotto_id INT(11) NOT NULL,
    prodotto_correlato_id INT(11) NOT NULL,
    UNIQUE ( prodotto_id, prodotto_correlato_id ),
    PRIMARY KEY(id),
    FOREIGN KEY(prodotto_id) REFERENCES prodotto(id),
    FOREIGN KEY(prodotto_correlato_id) REFERENCES prodotto(id)
);

CREATE TABLE video (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL UNIQUE,
    file VARCHAR(255) NOT NULL,
    prodotto_id INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(prodotto_id) REFERENCES prodotto(id),
    UNIQUE (nome, prodotto_id)
);

CREATE TABLE avatar (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL UNIQUE,
    file VARCHAR(255) NOT NULL,
    prodotto_id INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(prodotto_id) REFERENCES prodotto(id),
    UNIQUE (nome, prodotto_id)
);

CREATE TABLE immagine_rotazione (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL UNIQUE,
    file VARCHAR(255) NOT NULL,
    prodotto_id INT(11) NOT NULL,
    posizione INT(1) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(prodotto_id) REFERENCES prodotto(id),
    UNIQUE (nome, prodotto_id)
);

CREATE TABLE immagine_dettaglio (
    id INT(11) NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL UNIQUE,
    file VARCHAR(255) NOT NULL,
    prodotto_id INT(11) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(prodotto_id) REFERENCES prodotto(id),
    UNIQUE (nome, prodotto_id)
);

-- AUTUNNO, ESTATE, ..
INSERT INTO collezione ( nome ) VALUES ( 'AUTUNNO' );
INSERT INTO collezione ( nome ) VALUES ( 'ESTATE' );
INSERT INTO collezione ( nome ) VALUES ( 'INVERNO' );
INSERT INTO collezione ( nome ) VALUES ( 'PRIMAVERA' );