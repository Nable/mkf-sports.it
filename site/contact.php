<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/contact.php by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:59 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_v.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">

	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="clearfix">
		
		<div class="article-wrapper clearfix">
			
			<!-- CONTENT -->
			<article class="main">
			
				<h1>Contact Form Template</h1>
				
				<p>In eu tortor est. Aenean nulla metus, bibendum in lobortis vitae, lacinia quis nulla. Nam ullamcorper blandit lectus, quis ultrices ipsum pharetra in. Maecenas ac magna justo. Cras facilisis tristique turpis, vestibulum egestas odio consectetur a. Aenean dui urna, luctus vitae posuere quis, vulputate vel turpis.</p>
			
				<div id="respond">
					
											
					<form action="#" id="contactForm" method="post">
						
						<fieldset>
							
							<label for="contactName"><span>*</span> Name</label>
														<input type="text" name="contactName" id="contactName" value="" tabindex="1" />
							
							<label for="email"><span>*</span> Email Address</label>
														<input type="text" name="email" id="email" value="" />
							
							<label for="commentsText"><span>*</span> Message</label>
														<textarea name="comments" id="commentsText" rows="1" cols="1"></textarea>
							
							<input type="checkbox" name="sendCopy" id="sendCopy" class="checkbox" value="true" />
							<label class="checkbox" for="sendCopy">Send copy of email to yourself</label>
							
							<input name="submit" type="submit" id="submit" class="comment button" tabindex="5" value="Send" />
							
							<span class="displace"><label for="checking">If you want to submit this form, do not enter anything in this field</label>
							<input type="text" name="checking" id="checking" value="" /></span>
							<input type="hidden" name="submitted" id="submitted" value="true" />
							
						</fieldset>

					</form>
					
				</div>
				
			</article>
			
			<!-- SIDEBAR -->
			<ul id="article-sidebar">
				
				<!-- SELECT FIELDS -->
				<li id="categories-4" class="widget widget_categories">
				
					<h3 class="widgettitle">Categories</h3>
					
					<select name="cat" id="cat">
						<option value="-1">Select Category</option>
						<option>Child Category I</option>
						<option>Child Category II</option>
						<option>Child Category III</option>
						<option>Grandchild Category I</option>
						<option>Parent Category I</option>
						<option>Parent Category II</option>
						<option>Parent Category III</option>
					</select>
				
				</li>
				
				<!-- TEXT WIDGET -->
				<li class="widget widget_text">
				
					<h3 class="widgettitle">Address</h3>
					
					<div class="textwidget">
						<p><strong>Company Name</strong></p>
						<p>140 Church Street</p>
						<p>London</p>
						<p>England</p>
					</div>
				
				</li>
				
				<!-- SOCIAL BUTTONS -->
				<li class="widget raw_social">
					
					<h3 class="widgettitle">Social Buttons</h3>
					
					<div class="social-button-holder">
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>						
					</div>
				
				</li>
			
			</ul>			
			
		</div>

	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/contact.php by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:59 GMT -->
</html>