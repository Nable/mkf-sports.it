
	function show_share() { 
		$('#share-box').fadeIn(200) ; 
	}

	function hide_share() { 
		$('#share-box').fadeOut(200) ; 
	}
	
	function mainmenu(){
	$("#navigation a").removeAttr("title");
	$("#navigation ul").css({display: "none"}); // Opera Fix
	$("#navigation li").hover(function(){
			$(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
			},function(){
			$(this).find('ul:first').css({visibility: "hidden"});
			});
	}

	$(document).ready(function($) {
		
		mainmenu(); // Navigation
		
		$("#article-list").wrapInner(document.createElement("tr"));
		$("#article-list").wrapInner(document.createElement("table"));
		$("#article-list .article-wrapper").wrap("<td>");
		
		// IE FIXES
		$("#navigation li:last-child").addClass("ie-last-child-fix");
		
		// Share box animation
		$('.share-button').mouseenter(function() {
			show_share()
		});

		$('#share').mouseleave(function() {
			hide_share();
		});
		
		// Share button animation
		$('#share-box a, .social-button-holder a').hover(function(){
			$(this).stop().animate({ opacity : 0.5 }, 200 );
			},
			function () {
				$(this).stop().animate({ opacity : 1 }, 200 );
			}
		);
		
		// Nav menu collapse
		$("#expand-button").click(function () {   		
			$(this).toggleClass('collapse').parent('nav').find('ul').slideToggle();
		});
		
		// ---------- Toggle Content ---------- //
		$(".expanding .expand-button").click(function () {
			$(this).toggleClass('close').parent('div').find('.expand').slideToggle('slow');
		});
		
		// Image hover
		$('.hover').hover(function(){
			$('img', this).stop().animate({ opacity : 0 }, 500 );
			},
			function () {
				$('img', this).stop().animate({ opacity : 1 }, 500 );
			}
		);
		
		// Horizontal Portfolio Sorting
		$('ul#filter a').click(function() {
			$('td').fadeOut(500);
			
			$('ul#filter .current').removeClass('current');
			$(this).parent().addClass('current');

			var filterVal = this.getAttribute('title') ;
			
			if(filterVal == 'all') {
				$('td.hidden').animate({foo: 1}, 499).fadeIn(500);
				$('td').fadeIn(500);
				$('td.hidden').removeClass('hidden');
			} else {
				$('td').each(function() {
					if(!$(".article-wrapper", this).hasClass(filterVal)) {
						$(this).addClass('hidden');
					} else {
						$(this).animate({foo: 1}, 499).fadeIn(500);
						$(this).removeClass('hidden');
					}
				});
			}

			return false;

		});
		
		// Grid Portfolio Sorting
		$('ul#filter a').click(function() {
			$('#portfolio li').fadeOut(500);
			
			$('ul#filter .current').removeClass('current');
			$(this).parent().addClass('current');

			var filterVal = this.getAttribute('title') ;
			
			if(filterVal == 'all') {
				$('#portfolio li.hidden').animate({foo: 1}, 499).fadeIn(500);
				$('#portfolio li').fadeIn(500);
				$('#portfolio li.hidden').removeClass('hidden');
			} else {
				$('#portfolio li').each(function() {
					if(!$(this).hasClass(filterVal)) {
						$(this).addClass('hidden');
					} else {
						$(this).animate({foo: 1}, 499).fadeIn(500);
						$(this).removeClass('hidden');
					}
				});
			}

			return false;

		});
		
		
		// CONTACT VALIDATION
		
		// Setup
		$('#respond #submit').before('<h3 class="error">Oops!</h3><ul class="error"></ul>')
		$('.error').hide();
		
		var hasMessageError = false;
		var hasNameError = false;
		var hasEmailError = false;
		
		if (document.getElementById("comments")) { 
			hasMessageError = true;
		};
		
		if (document.getElementById("contactName")) { 
			hasNameError = true;
		};
		
		if (document.getElementById("email")) { 
			hasEmailError = true;
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		};
		
		
		// Validation
		$("#commentsText").blur(function(){				   				   
			$("#comment-error").remove();
			hasMessageError = false;
			
			var messageVal = $("#commentsText").val();
			if(messageVal == '') {
				$('.error').slideDown('normal');
				$('<li id="comment-error">You forgot to include your comment.</li>').appendTo('ul.error');
				hasMessageError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				$('.error').hide('normal');
			}
		});
		
		$("#contactName").blur(function(){					   				   
			$("#author-error").remove();
			hasNameError = false;
			
			var authorVal = $("#contactName").val();
			if(authorVal == '') {
				$('.error').slideDown('normal');
				$('<li id="author-error">You forgot to include your name.</li>').appendTo('ul.error');
				hasNameError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				$('.error').hide('normal');
			}
		});
		
		$("#email").blur(function(){					   				   
			$("#email-error").remove();
			hasEmailError = false;
			
			var emailVal = $("#email").val();
			if(emailVal == '') {
				$('.error').slideDown('normal');
				$('<li id="email-error">You forgot to include your email address.</li>').appendTo('ul.error');
				hasEmailError = true;
			} else if (!emailReg.test(emailVal)) {
				$('.error').slideDown('normal');
				$('<li id="email-error">The email address you entered is not valid.</li>').appendTo('ul.error');
				hasEmailError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				$('.error').hide('normal');
			}
		});
		
		//Comment Form Submit
		$('form#commentform').submit(function() {
		
			var messageVal = $("#commentsText").val();
			var authorVal = $("#contactName").val();
			var emailVal = $("#email").val();
			
			if (hasMessageError == false && hasNameError == false && hasEmailError == false) {
				
				if ( url.value == 'Website' ){ 
					url.value = '';
				}
				
				window.navigate("#");
				
			} else {
			
				return false;
			
			}	
			
		});
		
		//Contact Form Submit
		$('form#contactForm').submit(function() {
		
			var messageVal = $("#commentsText").val();
			var authorVal = $("#contactName").val();
			var emailVal = $("#email").val();
			
			if (hasMessageError == false && hasNameError == false && hasEmailError == false) {

				$('form#contactForm #submit').fadeOut('normal', function() {
					$(this).parent().append('<img id="loading" src="images/loader.gif" alt="Loading"/>');
				});
				var formInput = $(this).serialize();
				$.post($(this).attr('action'),formInput, function(data){
					$('form#contactForm').slideUp("fast", function() {				   
						$(this).before('<h3>Thanks!</h3><p>Your message was successfully sent.</p>');
					});
				});
			}
			
			return false;
			
		});
		
	});	