<?php
/* ---------------------------------------------------------------
 *              FUNZIONE DI CONNESSIONE AL DATABASE
 * ---------------------------------------------------------------*/
function connect()
{
    if ($link = mysql_connect(DBHOST,DBUSER,DBPASS))
    {
        if (!mysql_select_db(DBNAME, $link))
            return FALSE;
    }
    else return FALSE;
    
    return $link;
}

function close( $link )
{
    if ( $link )
        @mysql_close( $link );
}

/*_________________________________________________________________*/


/* ---------------------------------------------------------------
 *              FUNZIONE DI UTILITA' PER DATABASE
 * ---------------------------------------------------------------*/

function exist ($table_name,$field_name,$field_value)
{
    $link = connect();
    if ( $link )
    {
        $sql  = "SELECT ".$table_name.".".$field_name." FROM ".$table_name." ";
        $sql .= "WHERE ".$field_name."= '".$field_value."'";
                
        $res = mysql_query($sql);
        
        if (!$res)
            return false;
        
        if (mysql_num_rows($res)>0)
            return true;
        
        return false;
    }
    
    return mysql_error();
}

function estrai_oggetto( $risultato ) {
    if ( $risultato ) {
        $r = mysql_fetch_object( $risultato );

        return $r;
    }

    return FALSE;
}
/*_________________________________________________________________*/


/* ---------------------------------------------------------------
 *              LOGIN
 * ---------------------------------------------------------------*/
function query_login($user,$pass,$table)
{
    $ret = 0;
    $link = connect();
    if ( $link ) {
        if (($user == '') OR ($pass == ''))
            return $ret;

        $md5_pass = md5($pass);
        $sqlQuery = "SELECT * FROM %s WHERE username='%s' AND password='%s'";
        $query = sprintf($sqlQuery,$table,$user,$md5_pass);

        $result = mysql_query($query);

        if ( ( mysql_num_rows($result) == 1) ){
            $ret = 1;

            $sql_query_last_access = "UPDATE ".$table." SET ultimo_accesso = NOW() WHERE username='".$user."'";
            $result_last_access = mysql_query($sql_query_last_access);
        }
        
        close( $link );
    }

    return $ret;
}

/*Funzione di modifica della password*/
function modifica_password($p_old,$p_new)
{
    $link = connect();
    
    if ( $link )
    {
        $sqlQuery = 'UPDATE admin_user SET password=\'%s\' WHERE password=\'%s\'';
        $query    = sprintf($sqlQuery,$p_new,$p_old);
        $result   = mysql_query($query);
    
        if ( ( mysql_affected_rows() == 0) )
            return PASS_ERR;
        else if ( ( mysql_affected_rows() == 1) )
            return PASS_OK;
    }

    @mysql_close($link);
}

// LETTURA DEI DATI DALLA TABELLA DI LOG
function get_logs() {
    $link = connect();
    if ( $link ) {
        $sqlQuery = 'SELECT titolo, testo, table_ref, op_type, insert_at FROM admin_log ORDER BY insert_at DESC LIMIT ' .LOG_ROW_NUMBER;
        $result = mysql_query($sqlQuery);
    }
    
    close( $link );
    return $result;
}

// Si aspetta che l'attributo $op_type sia valorizzato ad uno dei seguenti 
// ADMIN_OP_TYPE_ADD / ADMIN_OP_TYPE_CHANGE / ADMIN_OP_TYPE_DELETE
function insert_log( $titolo, $table_ref, $op_type ) {
    $link = connect();

    if ( $link )
    {
	    $testo = '';
	    if ( $op_type == ADMIN_OP_TYPE_ADD )
	        $testo = 'Aggiunta riga in '.$table_ref.'.';
	    else if ( $op_type == ADMIN_OP_TYPE_CHANGE )
	        $testo = 'Modificata riga in '.$table_ref.'.';
	    else if ( $op_type == ADMIN_OP_TYPE_DELETE )
	        $testo = 'Cancellata riga in '.$table_ref.'.';

		$insert_at = date("Y-m-d H:i:s");

        $sql  = "INSERT INTO admin_log( titolo, testo, table_ref, op_type, insert_at ) VALUES ('".$titolo."', '".$testo."', '".$table_ref."', '".$op_type."', '".$insert_at."')";

        $res = mysql_query( $sql );

        if ( !$res ) {
            mysql_close( $link );
            return 0;
        }
		
        if ( @mysql_num_rows( $res ) == 1) {
            $ret = estrai_oggetto( $res );
            mysql_close( $link );
            return $ret->count;
        }

        @mysql_close( $link );
        return 0;
    }

    return 0;
}

function getNumRows( $table_name ){
    $link = connect();
    if ( $link )
    {
        $sql  = "SELECT count(*) AS count FROM ".$table_name;
                
        $res = mysql_query( $sql );
        
        if ( !$res ) {
            @mysql_close( $link );
            return 0;
        }

        if ( mysql_num_rows( $res ) == 1) {
            $ret = estrai_oggetto( $res );
            @mysql_close( $link );
            return $ret->count;
        }

        @mysql_close( $link );
        return 0;
    }

    return 0;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Categorie
/*_________________________________________________________________*/
function getCountCategoriaByTitoloAndClientela( $link, $categoria_titolo, $clientela_id ) {
    $ret = 0;
	
    if ( $link ) {
	$sql = "SELECT count(*) as c FROM categoria WHERE titolo='".$categoria_titolo."' AND clientela_id='".$clientela_id."'";
		
	$res = mysql_query($sql);
		
	if ( $res != null and $res != ' ' ) {
	    while ( $row = mysql_fetch_array($res) )
    		$ret = $row['c'];
	}
		
	if ( $ret == null or $ret == ' ' )
	    $ret = 0;
		
	    return $ret;
    }
	
    return false;
}

function getCategorieEClientela() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ca.id AS id, ca.titolo AS ca_t, cl.titolo AS cl_t FROM categoria ca JOIN clientela cl ON ca.clientela_id=cl.id ORDER BY ca.titolo";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $ca_t = $row['ca_t'];
            $cl_t = $row['cl_t'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_categorie.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$ca_t."</a></th><td class='nowrap'>".$cl_t."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function addCategoria( $categoria_titolo, $clientela_id ) {
    $link = connect();

    $ret = false;
    if ( $link ) {
        if ( getCountCategoriaByTitoloAndClientela( $link, $categoria_titolo, $clientela_id ) == 0 ) {
            $sql = "INSERT INTO categoria (titolo, clientela_id) VALUES ('" .$categoria_titolo. "', " .$clientela_id. ")";

            if ( mysql_query( $sql ) )
                $ret = true;

            @mysql_close( $link );
        }
    }
    
    return $ret;
}

function updateCategoria( $categoria_titolo, $categoria_id, $clientela_id ) {
    $link = connect();
    $ret = false;

    if ( $link ) {
        if ( getCountCategoriaByTitoloAndClientela( $link, $categoria_titolo, $clientela_id ) === 0 ) {
            $sql = "UPDATE categoria SET titolo='".$categoria_titolo."', clientela_id=".$clientela_id." WHERE id=".$categoria_id;

            if ( mysql_query( $sql ) )
                $ret = true;

	        @mysql_close( $link );
        }
    }

    return $ret;
}

function delCategoria( $categoria_id ) {
    $link = connect();
    $ret = false;
    if ( $link and isset($categoria_id) and $categoria_id != "" ) {
        $categoria_da_cancellare = getTitoloCategoriaByIdToDelete( $link, $categoria_id );
        $sql = "DELETE FROM categoria WHERE id = " .$categoria_id;
        
        if ( mysql_query( $sql ) ) {
            $ret = true;
			insert_log( $categoria_da_cancellare, CATEGORIA_TABLE, ADMIN_OP_TYPE_DELETE );
		}

        @mysql_close($link);
    }
    
    return $ret;
}

function getTitoloCategoriaById( $categoria_id ) {
	$link = connect();
    if ( $link and isset( $categoria_id ) and $categoria_id != "" ) {
        $sql = "SELECT titolo FROM categoria WHERE id = " .$categoria_id;
        
        $res = mysql_query( $sql );
        
        $ret = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['titolo'];
		@mysql_close($link);
		return $ret;
    }
    
    return false;
}

function getTitoloCategoriaByIdToDelete( $link, $categoria_id ) {
    if ( $link and isset( $categoria_id ) and $categoria_id != "" ) {
        $sql = "SELECT titolo FROM categoria WHERE id = " .$categoria_id;
        
        $res = mysql_query( $sql );
        
        $ret = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['titolo'];

        return $ret;
    }
    
    return false;
}

function getClientelaIdFromCategoriaById( $categoria_id ) {
    $link = connect();
    if ( $link and isset( $categoria_id ) and $categoria_id != "" ) {
        $sql = "SELECT clientela_id FROM categoria WHERE id = " .$categoria_id;
        
        $res = mysql_query( $sql );
        
        $ret = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['clientela_id'];
        
        @mysql_close($link);
        return $ret;
    }
    
    return false;
}

/*_________________________________________________________________*/
// Queries su Clientela
/*_________________________________________________________________*/
function getCountClientelaByTitolo( $link, $clientela_titolo ) {
	$ret = 0;

	if ( $link ) {
		$sql = "SELECT count(*) as c FROM clientela WHERE titolo='".$clientela_titolo."'";

		$res = mysql_query($sql);

		if ( $res != null and $res != ' ' ) {
			while ( $row = mysql_fetch_array($res) )
    		    $ret = $row['c'];
		}

		if ( $ret == null or $ret == ' ' )
		   $ret = 0;

		return $ret;
	}

	return false;
}

function addClientela( $clientela_titolo ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	    if ( getCountClientelaByTitolo( $link, $clientela_titolo ) == 0) {
	        $sql = "INSERT INTO clientela( titolo ) VALUES ('" .$clientela_titolo. "' )";

	        if ( mysql_query( $sql ) )
	            $ret = true;

	        @mysql_close( $link );
	    }
    }
    
    return $ret;
}

function getClienteleOptions() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, titolo FROM clientela ORDER BY titolo";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret_row = $ret_row."<option value='".$row['id']."'>".$row['titolo']."</option>";
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getClientelaSelectedById( $clientela_id ) {
    $link = connect();

    if ( $link )
    {
        $sql = "SELECT id, titolo FROM clientela ORDER BY titolo";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) ) {
            if ( $row['id'] == $clientela_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row['titolo']."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row['titolo']."</option>";

        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getTitoloClientelaById( $clientela_id ) {
	 $link = connect();
    if ( $link and isset( $clientela_id ) and $clientela_id != "" ) {
        $sql = "SELECT titolo FROM clientela WHERE id = " .$clientela_id;

        $res = mysql_query( $sql );

        $ret = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['titolo'];
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getTitoloClientelaByIdToDelete( $link, $clientela_id ) {

    if ( $link and isset( $clientela_id ) and $clientela_id != "" ) {
        $sql = "SELECT titolo FROM clientela WHERE id = " .$clientela_id;

        $res = mysql_query( $sql );

        $ret = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['titolo'];
        
        return $ret;
    }
    
    return false;
}

function getClientela() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, titolo FROM clientela ORDER BY titolo";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $titolo = $row['titolo'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$titolo."</a></th></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function updateClientela( $clientela_id, $titolo ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	    if ( getCountClientelaByTitolo( $link, $titolo ) == 0 ) {
	        $sql = "UPDATE clientela SET titolo='".$titolo."' WHERE id=".$clientela_id;

	        if (mysql_query( $sql ) )
				$ret = true;

	        @mysql_close($link);
	    }
    }
    
    return $ret;
}

function delClientela( $clientela_id ) {
	$link = connect();
	$ret = false;

    if ( $link and isset( $clientela_id)  and $clientela_id != "" ) {
    	$clientela_da_cancellare = getTitoloClientelaByIdToDelete( $link, $clientela_id );

        $sql = "DELETE FROM clientela WHERE id = " .$clientela_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $clientela_da_cancellare, CLIENTELA_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Colore
/*_________________________________________________________________*/
function getCountColoreByNomeECodice( $link, $nome, $codice ) {
	$ret = 0;
	
	if ( $link ) {
		$sql = "SELECT count(*) as c FROM colore WHERE nome='".$nome."' OR codice='".$codice."'";
		
		$res = mysql_query($sql);
		
		if ( $res != null and $res != ' ' ) {
			while ( $row = mysql_fetch_array($res) )
    		    $ret = $row['c'];
		}
		
		if ( $ret == null or $ret == ' ' )
		   $ret = 0;

		return $ret;
	}
	
	return false;
}

function addColore( $nome, $codice ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	    if ( getCountColoreByNomeECodice( $link, $nome, $codice ) == 0 ) {
	        $sql = "INSERT INTO colore ( nome, codice ) VALUES ('" .$nome. "', '" .$codice. "' )";

	        if ( mysql_query( $sql ) )
				$ret = true;

	        @mysql_close( $link );

	        return $ret;
		}
    }
    
    return $ret;
}

function getColore() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome, codice FROM colore ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $codice = $row['codice'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_colori.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td><img src='".$codice."' height='15'  width='15'/></td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getColoreFileById($id){
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT codice FROM colore WHERE id = ".$id;
        $res = mysql_query( $sql );

        $ret = '';

        while ( $row = mysql_fetch_array( $res ) ) 
            $ret = $row['codice'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getNomeCodiceColoreByIdToDelete( $link, $colore_id ) {
    if ( $link and isset( $colore_id ) and $colore_id != "" ) {
        $sql = "SELECT nome, codice FROM colore WHERE id = " .$colore_id;

        $res = mysql_query( $sql );

        $ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret['nome'] = $row['nome'];
            $ret['codice'] = $row['codice'];
        }
        
        return $ret;
    }
    
    return false;
}

function getNomeCodiceColoreById( $colore_id ) {
    $link = connect();
	if ( $link and isset( $colore_id ) and $colore_id != "" ) {
        $sql = "SELECT nome, codice FROM colore WHERE id = " .$colore_id;

        $res = mysql_query( $sql );

        $ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret['nome'] = $row['nome'];
            $ret['codice'] = $row['codice'];
        }
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function updateColore( $colore_id, $nome, $codice ) {
    $link = connect();
    $ret = false;

    if ( $link ) {
        $sql = "UPDATE colore SET nome='".$nome."', codice='".$codice."' WHERE id=".$colore_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function delColore( $colore_id ) {
    $link = connect();
    $ret = false;

    if ( $link and isset( $colore_id)  and $colore_id != "" ) {
	    $colore_da_cancellare = getNomeCodiceColoreByIdToDelete( $link, $colore_id );
        $sql = "DELETE FROM colore WHERE id = " .$colore_id;

        if ( mysql_query( $sql ) ) {
            insert_log( $colore_da_cancellare['nome'], COLORE_TABLE, ADMIN_OP_TYPE_DELETE );
            $ret = true;
        }

        @mysql_close($link);
    }
    
    return $ret;
}

function getColoriOptions() {
    $link = connect();
    if ( $link )
    {
    	$sql = "SELECT id, nome FROM colore ORDER BY nome";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        
        if ( $res != "" ){
	        while ( $row = mysql_fetch_array( $res ) )
	            $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']."</option>";
        }

        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Avatar
/*_________________________________________________________________*/
function getAvatars() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ir.nome, ir.id, pg.nome as prodotto_id FROM avatar ir JOIN prodotto p ON p.id=ir.prodotto_id join prodotto_generico pg on p.prodotto_generico_id = pg.id ORDER BY ir.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $prodotto_id = $row['prodotto_id'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_avatars.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$prodotto_id."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getAvatarById( $id ) {
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM avatar where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getProdottoOptions() {
    $link = connect();
    if ( $link )
    {
    	$sql = "SELECT a.id, b.nome, col.nome FROM prodotto a JOIN prodotto_generico b ON a.prodotto_generico_id = b.id 
				JOIN colore col ON a.colore_id = col.id ORDER BY b.nome";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        
        if ( $res != "" ){
	        while ( $row = mysql_fetch_array( $res ) )
	            $ret_row = $ret_row."<option value='".$row['id']."'>".$row[1]." - ".$row[2]."</option>";
        }

        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}


function addAvatar( $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
    	$sql = "INSERT INTO avatar (nome, prodotto_id, file) VALUES ('" .$avatar_nome. "', " .$prodotto_id. ",'".$avatar_file."')";
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function getProdottoSelectedById( $prodotto_id ) {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT a.id, b.nome, col.nome FROM prodotto a JOIN prodotto_generico b ON a.prodotto_generico_id = b.id 
				JOIN colore col ON a.colore_id = col.id WHERE a.id = ".$prodotto_id." ORDER BY b.nome";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) ) {
            if ( $row['id'] == $prodotto_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row[1]." - ".$row[2]."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row[1]." - ".$row[2]."</option>";

        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function updateAvatar( $id, $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
        $sql = "UPDATE avatar SET nome='".$avatar_nome."', file='".$avatar_file."', prodotto_id = ".$prodotto_id." WHERE id=".$id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function delAvatar( $avatar_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset( $avatar_id)  and $avatar_id != "" ) {
	    $avatar_da_cancellare = getAvatarByIdToDelete( $link, $avatar_id );
        $sql = "DELETE FROM avatar WHERE id = " .$avatar_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $avatar_da_cancellare['nome'], AVATAR_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getAvatarByIdToDelete( $link, $id ) {
    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM avatar where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
        
        return $ret;
    }
    
    return false;
}

function getAvartFileById($id){
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT file FROM avatar WHERE id = ".$id;
        $res = mysql_query( $sql );

        $ret = '';

        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['file'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Prodotti Generici
/*_________________________________________________________________*/
function getCategorieOptions() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ca.id AS id, ca.titolo AS titolo, cli.titolo AS clientela FROM categoria ca JOIN clientela cli ON ca.clientela_id=cli.id ORDER BY titolo";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret_row = $ret_row."<option value='".$row['id']."'>".$row['titolo']." - ".$row['clientela']."</option>";
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getCollezioneOptions() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome FROM collezione ORDER BY nome";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) )
            $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']."</option>";
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getProdottoGenericoById( $prodotto_generico_id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT nome, descrizione, attivo, categoria_id, collezione_id FROM prodotto_generico WHERE id=".$prodotto_generico_id;
        $res = mysql_query( $sql );
        
        $index = 0;
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'] = $row['nome'];
            $array_ret['descrizione'] = $row['descrizione'];
            $array_ret['attivo'] = $row['attivo'];
            $array_ret['categoria_id'] = $row['categoria_id'];
            $array_ret['collezione_id'] = $row['collezione_id'];
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return false;
}

function getProdottoGenericoByIdToDelete( $link, $prodotto_generico_id ) {
    if ( $link )
    {
        $sql = "SELECT nome, descrizione, attivo, categoria_id FROM prodotto_generico WHERE id=".$prodotto_generico_id;
        $res = mysql_query( $sql );
        
        $index = 0;
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'] = $row['nome'];
            $array_ret['descrizione'] = $row['descrizione'];
            $array_ret['attivo'] = $row['attivo'];
            $array_ret['categoria_id'] = $row['categoria_id'];
        }
        
        return $array_ret;
    }
    
    return false;
}

function getCategoriaSelectedById( $categoria_id ) {
    
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, titolo FROM categoria ORDER BY titolo";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) )
            if ( $row['id'] == $categoria_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row['titolo']."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row['titolo']."</option>";
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getCollezioneSelectedById( $collezione_id ) {
    
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome FROM collezione ORDER BY nome";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) )
            if ( $row['id'] == $collezione_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row['nome']."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']."</option>";
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getProdottoGenericoECategoria() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT pg.id AS id, pg.nome AS nome, pg.descrizione AS descrizione, pg.attivo AS attivo, ca.titolo AS cat FROM categoria ca JOIN prodotto_generico pg ON ca.id=pg.categoria_id ORDER BY pg.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $descrizione = $row['descrizione'];
            $attivo = $row['attivo'];
            $cat = $row['cat'];
            
            $row_attivo = '';

            if ( $attivo == 1 )
                $row_attivo = "<img src='".ADMIN_IMAGES_PATH."/icon-yes.gif' alt='1' />";
            else
                $row_attivo = "<img src='".ADMIN_IMAGES_PATH."/icon-no.gif' alt='0' />";

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$row_attivo."</td><td class='nowrap'>".$cat."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getCountProdottoGenericoByNomeCategoriaId( $link, $nome, $categoria_id, $collezione_id ) {
	$ret = 0;
	
	if ( $link ) {
		$sql = "SELECT count(*) as c FROM prodotto_generico WHERE nome='".$nome."' AND categoria_id=".$categoria_id." AND collezione_id = ".$collezione_id;

		$res = mysql_query($sql);
		
		if ( $res != null and $res != ' ' )
			while ( $row = mysql_fetch_array($res) )
    		    $ret = $row['c'];
		
		if ( $ret == null or $ret == ' ' )
		   $ret = 0;
		
		return $ret;
	}
	
	return false;
}

function addProdottoGenerico( $nome, $descrizione, $attivo, $categoria_id, $collezione_id ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	    if ( getCountProdottoGenericoByNomeCategoriaId( $link, $nome, $categoria_id, $collezione_id ) == 0 ) {

		    if ( $attivo == 'on' )
		        $attivo = 1;
		    else
		        $attivo = 0;

            $sql = "INSERT INTO prodotto_generico (nome, descrizione, attivo, categoria_id, collezione_id) VALUES ('" .$nome. "', '" .$descrizione. "', ".$attivo.", ".$categoria_id.", ".$collezione_id.")";

			if ( mysql_query( $sql ) )
				$ret = true;
        
       		@mysql_close($link);
        }
    }
    
    return $ret;
}

function updateProdottoGenerico( $prodotto_generico_id, $nome, $descrizione, $attivo, $categoria_id ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
		if ( $attivo == 'on' )
			$attivo = 1;
		else
			$attivo = 0;

		    $sql = "UPDATE prodotto_generico SET nome='".$nome."', descrizione='".$descrizione."', attivo=".$attivo.", categoria_id=".$categoria_id." WHERE id=".$prodotto_generico_id;

	        if ( mysql_query( $sql ) )
				$ret = true;
        
    	    @mysql_close($link);
    }

    return $ret;
}

function delProdottoGenerico( $prodotto_generico_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset($prodotto_generico_id) and $prodotto_generico_id != "" ) {
	    $prodotto_da_cancellare = getProdottoGenericoByIdToDelete( $link, $prodotto_generico_id );

        $sql = "DELETE FROM prodotto_generico WHERE id = " .$prodotto_generico_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $prodotto_da_cancellare['nome'], PRODOTTO_GENERICO_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getProdottiGenericiOptions() {
    $link = connect();

    if ( $link )
    {
    	$sql = "SELECT pg.id AS id, pg.nome AS nome, c.titolo AS cat FROM prodotto_generico pg JOIN categoria c ON pg.categoria_id = c.id ORDER BY nome";
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        
        if ( $res != "" ){
	        while ( $row = mysql_fetch_array( $res ) )
	            $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']." - ".$row['cat']."</option>";
        }

        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Istanze di prodotto
/*_________________________________________________________________*/
function getIstanzaProdottoById( $istanza_id ) {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT attivo, prodotto_generico_id, colore_id FROM prodotto WHERE id=".$istanza_id;
        $res = mysql_query( $sql );
        
        $index = 0;
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['attivo'] = $row['attivo'];
            $array_ret['prodotto_generico_id'] = $row['prodotto_generico_id'];
            $array_ret['colore_id'] = $row['colore_id'];
        }
        
        @mysql_close( $link );
        return $array_ret;
    }
    
    return false;
}

function getProdottoGenericoSelectedById( $prodotto_generico_id ) {
    
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome FROM prodotto_generico ORDER BY nome";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) ) {
            if ( $row['id'] == $prodotto_generico_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row['nome']."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']."</option>";
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getColoreSelectedById( $colore_id ) {
    
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome FROM colore ORDER BY nome";
        $res = mysql_query( $sql );

        $ret_row = '';

        $ret_row = $ret_row."<option value='".$row['id']."'>---------</option>";
        while ( $row = mysql_fetch_array( $res ) ) {
            if ( $row['id'] == $colore_id )
                $ret_row = $ret_row."<option value='".$row['id']."' selected='selected'>".$row['nome']."</option>";
            else
                $ret_row = $ret_row."<option value='".$row['id']."'>".$row['nome']."</option>";

        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getIstanzaDiProdottoConColore() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT p.id AS id, pg.nome AS nome, p.attivo AS attivo, ca.titolo AS cat, co.nome AS colore FROM categoria ca JOIN prodotto_generico pg ON ca.id=pg.categoria_id JOIN prodotto p ON pg.id = p.prodotto_generico_id JOIN colore co ON p.colore_id=co.id ORDER BY pg.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $attivo = $row['attivo'];
            $cat = $row['cat'];
            $colore = $row['colore'];
            
            $row_attivo = '';

            if ( $attivo == 1 )
                $row_attivo = "<img src='".ADMIN_IMAGES_PATH."/icon-yes.gif' alt='1' />";
            else
                $row_attivo = "<img src='".ADMIN_IMAGES_PATH."/icon-no.gif' alt='0' />";

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$cat."</td><td class='nowrap'>".$colore."</td><td class='nowrap'>".$row_attivo."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getCountIstanzaProdottoByProdottoGenericoIdColoreId( $link, $prodotto_generico_id, $colore_id ) {
	$ret = 0;
	
	if ( $link ) {
		$sql = "SELECT count(*) as c FROM prodotto WHERE prodotto_generico_id=".$prodotto_generico_id." AND colore_id=".$colore_id;

		$res = mysql_query($sql);
		
		if ( $res != null and $res != ' ' )
			while ( $row = mysql_fetch_array($res) )
    		    $ret = $row['c'];
		
		if ( $ret == null or $ret == ' ' )
		   $ret = 0;
		
		return $ret;
	}
	
	return false;
}

function updateIstanzaProdotto( $istanza_id, $prodotto_generico_id, $colore_id, $attivo, $prodotti_correlati ) {
    $link = connect();
	$ret = false;

    if ( $link ) {

	    if ( $attivo == 'on' )
	        $attivo = 1;
	    else
	        $attivo = 0;

            $sql = "UPDATE prodotto SET attivo=" .$attivo. ", prodotto_generico_id=" .$prodotto_generico_id. ", colore_id=".$colore_id." WHERE id=".$istanza_id;

	        if ( mysql_query( $sql ) )
				$ret = true;

			$sql = "DELETE FROM prodotto_correlato WHERE prodotto_id=".$istanza_id;
			mysql_query( $sql );

			foreach( $prodotti_correlati AS $prodotto_correlato_id ) {
				$sql = "INSERT INTO prodotto_correlato SET prodotto_id=".$istanza_id.", prodotto_correlato_id=".$prodotto_correlato_id;
				mysql_query( $sql );	
			}
            @mysql_close($link);
    }
    
    return $ret;
}

function addIstanzaProdotto( $prodotto_generico_id, $colore_id, $attivo) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	    if ( getCountIstanzaProdottoByProdottoGenericoIdColoreId( $link, $prodotto_generico_id, $colore_id ) == 0 ) {
		    if ( $attivo == 'on' )
		        $attivo = 1;
		    else
		        $attivo = 0;

            $sql = "INSERT INTO prodotto (attivo, prodotto_generico_id, colore_id) VALUES (" .$attivo. ", " .$prodotto_generico_id. ", ".$colore_id.")";

	        if ( mysql_query( $sql ) )
				$ret = true;
        
    	    @mysql_close($link);
        }
    }
    
    return $ret;
}

function getProdottoGenericoIdByNome( $link, $nome ) {
    
    if ( $link )
    {
        $sql = "SELECT id FROM prodotto_generico WHERE nome='".$nome."'";
        $res = mysql_query( $sql );

		$ret = '';
        while ( $row = mysql_fetch_array( $res ) )
			$ret = $row['id'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getColoreIdByNome( $link, $nome ) {
    
    if ( $link )
    {
        $sql = "SELECT id FROM colore WHERE nome='".$nome."'";
        $res = mysql_query( $sql );

		$ret = '';
        while ( $row = mysql_fetch_array( $res ) )
			$ret = $row['id'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getIstanzaIdByProdottoId( $link, $istanza_id ) {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id FROM prodotto WHERE id=".$istanza_id;
        $res = mysql_query( $sql );
        
        $index = 0;
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) )
            $array_ret['id'] = $row['id'];
        
        return $array_ret;
    }
    
    return false;
}

function delIstanzaProdotto( $prodotto_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset($prodotto_id) and $prodotto_id != "" ) {
	    $prodotto_da_cancellare = getIstanzaIdByProdottoId( $link, $prodotto_id );

        $sql = "DELETE FROM prodotto WHERE id = " .$prodotto_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $prodotto_da_cancellare['id'], PRODOTTO_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Video
/*_________________________________________________________________*/
function getVideos() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ir.nome, ir.id, pg.nome as prodotto_id FROM video ir JOIN prodotto p ON p.id=ir.prodotto_id join prodotto_generico pg on p.prodotto_generico_id = pg.id ORDER BY ir.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $prodotto_id = $row['prodotto_id'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$prodotto_id."</td></tr>";

            $index++;
        }
        
        mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function addVideo( $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
    	$sql = "INSERT INTO video (nome, prodotto_id, file) VALUES ('" .$avatar_nome. "', " .$prodotto_id. ",'".$avatar_file."')";
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function updateVideo( $id, $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
        $sql = "UPDATE video SET nome='".$avatar_nome."', file='".$avatar_file."', prodotto_id = ".$prodotto_id." WHERE id=".$id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function delVideo( $avatar_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset( $avatar_id)  and $avatar_id != "" ) {
	    $avatar_da_cancellare = getVideoByIdToDelete( $link, $avatar_id );
        $sql = "DELETE FROM video WHERE id = " .$avatar_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $avatar_da_cancellare['nome'], VIDEO_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getVideoFileById($id){
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT file FROM video WHERE id = ".$id;
        $res = mysql_query( $sql );

        $ret = '';

        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['file'];
        
        mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getVideoById( $id ) {
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM video where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
		mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getVideoByIdToDelete( $link, $id ) {

    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM video where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
       
        return $ret;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Immagini Dettaglio
/*_________________________________________________________________*/
function getImmDettaglio() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ir.nome, ir.id, pg.nome as prodotto_id FROM immagine_dettaglio ir JOIN prodotto p ON p.id=ir.prodotto_id join prodotto_generico pg on p.prodotto_generico_id = pg.id ORDER BY ir.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $prodotto_id = $row['prodotto_id'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_dettaglio.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$prodotto_id."</td></tr>";

            $index++;
        }
        
        mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function addImmDettaglio( $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
    	$sql = "INSERT INTO immagine_dettaglio (nome, prodotto_id, file) VALUES ('" .$avatar_nome. "', " .$prodotto_id. ",'".$avatar_file."')";
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function updateImmDettaglio( $id, $avatar_nome, $prodotto_id, $avatar_file ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
        $sql = "UPDATE immagine_dettaglio SET nome='".$avatar_nome."', file='".$avatar_file."', prodotto_id = ".$prodotto_id." WHERE id=".$id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function delImmDettaglio( $avatar_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset( $avatar_id)  and $avatar_id != "" ) {
	    $avatar_da_cancellare = getImmDettaglioByIdToDelete( $link, $avatar_id );
        $sql = "DELETE FROM immagine_dettaglio WHERE id = " .$avatar_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $avatar_da_cancellare['nome'], IMM_DETTEGLIO_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getImmDettaglioFileById($id){
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT file FROM immagine_dettaglio WHERE id = ".$id;
        $res = mysql_query( $sql );

        $ret = '';

        while ( $row = mysql_fetch_array( $res ) ) 
            $ret = $row['file'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getImmDettaglioByIdToDelete( $link, $id ) {
    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM immagine_dettaglio WHERE id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
        
        return $ret;
    }
    
    return false;
}

function getImmDettaglioById( $id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM immagine_dettaglio WHERE id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Queries su Immagine Rotazione
/*_________________________________________________________________*/
function getImmRotazione() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT ir.nome, ir.id, pg.nome as prodotto_id FROM immagine_rotazione ir JOIN prodotto p ON p.id=ir.prodotto_id join prodotto_generico pg on p.prodotto_generico_id = pg.id ORDER BY ir.nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $prodotto_id = $row['prodotto_id'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_rotazione.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$nome."</a></th><td class='nowrap'>".$prodotto_id."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function addImmRotazione( $avatar_nome, $prodotto_id, $avatar_file, $posizione ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
    	$sql = "INSERT INTO immagine_rotazione (nome, prodotto_id, file, posizione) VALUES ('" .$avatar_nome. "', " .$prodotto_id. ",'".$avatar_file."', ".$posizione.")";
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function updateImmRotazione( $id, $avatar_nome, $prodotto_id, $avatar_file, $posizione ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
        $sql = "UPDATE immagine_rotazione SET nome='".$avatar_nome."', file='".$avatar_file."', prodotto_id = ".$prodotto_id.", posizione=".$posizione." WHERE id=".$id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function delImmRotazione( $avatar_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset( $avatar_id)  and $avatar_id != "" ) {
	    $avatar_da_cancellare = getImmRotazioneByIdToDelete( $link, $avatar_id );
        $sql = "DELETE FROM immagine_rotazione WHERE id = " .$avatar_id;
        
        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $avatar_da_cancellare['nome'], IMM_ROTAZIONE_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getImmRotazioneFileById($id){
	$link = connect();
    if ( $link )
    {
        $sql = "SELECT file FROM immagine_rotazione WHERE id = ".$id;
        $res = mysql_query( $sql );

        $ret = '';

        while ( $row = mysql_fetch_array( $res ) )
            $ret = $row['file'];
        
        @mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getImmRotazioneById( $id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id, posizione FROM immagine_rotazione where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
            $ret['posizione'] = $row['posizione'];
        }
		@mysql_close( $link );
        return $ret;
    }
    
    return false;
}

function getImmRotazioneByIdToDelete( $link, $id ) {
    if ( $link )
    {
        $sql = "SELECT id, nome, file, prodotto_id FROM immagine_rotazione where id =".$id." ORDER BY nome";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        
        while ( $row = mysql_fetch_array( $res ) ){
            $ret = Array();
        	$ret['id'] = $row['id'];
            $ret['nome'] = $row['nome'];
            $ret['file'] = $row['file'];
            $ret['prodotto_id'] = $row['prodotto_id'];
        }

        return $ret;
    }
    
    return false;
}
/*_________________________________________________________________*/

/*_________________________________________________________________*/
// Sezione Utenti
/*_________________________________________________________________*/
function getUtente() {
    $link = connect();
    if ( $link )
    {
        $sql = "SELECT id, nome, cognome, email, username, ultimo_accesso FROM admin_user ORDER BY username";
        
        $res = mysql_query( $sql );
        
        $index = 0;
        $ret_row = '';
        while ( $row = mysql_fetch_array( $res ) ) {
            $class = 'row1';
            if ( $index % 2 == 0)
                $class = 'row2';
            
            $id = $row['id'];
            $nome = $row['nome'];
            $cognome = $row['cognome'];
            $email = $row['email'];
            $username = $row['username'];
            $ultimo_accesso = $row['ultimo_accesso'];

            $ret_row = $ret_row."<tr class='" .$class. "'><td><input type='checkbox' class='action-select' value='" .$id. "' name='fields_to_del[]' /></td><th><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php?op=".ADMIN_OP_TYPE_CHANGE."&id=".$id."'>".$username."</a></th><td>".$nome."</td><td>".$cognome."</td><td>".$email."</td><td>".$ultimo_accesso."</td></tr>";

            $index++;
        }
        
        @mysql_close( $link );
        return $ret_row;
    }
    
    return false;
}

function getUtenteById( $utente_id ) {
    $link = connect();

    if ( $link and isset( $utente_id ) and $utente_id != "" ) {
        $sql = "SELECT nome, cognome, email, username FROM admin_user WHERE id = " .$utente_id;

        $res = mysql_query( $sql );

        $ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret['nome'] = $row['nome'];
            $ret['cognome'] = $row['cognome'];
            $ret['email'] = $row['email'];
            $ret['username'] = $row['username'];
        }
        
        @mysql_close($link);
        return $ret;
    }
    
    return false;
}
	
function updateUtente( $id, $nome, $cognome, $username, $email, $password ) {
    $link = connect();
	$ret = false;

    if ( $link ) {
	
		$md5_pass = md5( $password );
        $sql = "UPDATE admin_user SET nome='".$nome."', cognome='".$cognome."', email='".$email."', username='".$username."', password='".$md5_pass."' WHERE id=".$id;

        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);
    }
    
    return $ret;
}

function getCountUtenteByUsername( $link, $username ) {
	$ret = 0;
	
	if ( $link ) {
		$sql = "SELECT count(*) as c FROM admin_user WHERE username='".$username."'";
		
		$res = mysql_query($sql);
		
		if ( $res != null and $res != ' ' )
			while ( $row = mysql_fetch_array($res) )
    		    $ret = $row['c'];
		
		if ( $ret == null or $ret == ' ' )
		   $ret = 0;

		return $ret;
	}
	
	return false;
}

function addUtente( $nome, $cognome, $email, $username, $password ) {
    $link = connect();
    if ( $link ) {
	    if ( getCountUtenteByUsername( $link, $username ) == 0 ) {
		    $insert_at = date("Y-m-d H:i:s");
		
		    $md5_pass = md5( $password );
	        $sql = "INSERT INTO admin_user ( nome, cognome, username, email, password, ultimo_accesso ) VALUES ('" .$nome. "', '" .$cognome. "', '" .$username. "', '" .$email. "', '" .$md5_pass. "', '".$insert_at."' )";

	        if ( mysql_query( $sql ) )
				$ret = true;
   	     	
	        @mysql_close($link);
	        return $ret;
		}
    }
    
    return false;
}

function getUtenteToDeleteById( $link, $utente_id ) {

    if ( $link and isset( $utente_id ) and $utente_id != "" ) {
        $sql = "SELECT nome, cognome, email, username FROM admin_user WHERE id = " .$utente_id;

        $res = mysql_query( $sql );

        $ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret['nome'] = $row['nome'];
            $ret['cognome'] = $row['cognome'];
            $ret['email'] = $row['email'];
            $ret['username'] = $row['username'];
        }

        return $ret;
    }
    
    return false;
}

function delUtente( $utente_id ) {
    $link = connect();
	$ret = false;

    if ( $link and isset( $utente_id)  and $utente_id != "" ) {
	    $utente_da_cancellare = getUtenteToDeleteById( $link, $utente_id );
        $sql = "DELETE FROM admin_user WHERE id = " .$utente_id;

        if ( mysql_query( $sql ) )
			$ret = true;
        
        @mysql_close($link);

        insert_log( $utente_da_cancellare['username'], UTENTE_TABLE, ADMIN_OP_TYPE_DELETE );
    }
    
    return $ret;
}

function getNomiProdottiCorrelatoById( $prodotto_id, $prodotto_correlato_id ){
	$ret = 0;

    $array = Array();
	if ( $link ) {
		$sql = "SELECT id FROM prodotto_correlato WHERE prodotto_id=".$prodotto_id." AND prodotto_correlato_id=".$prodotto_correlato_id;

		$res = mysql_query($sql);

		if ( $res != null and $res != ' ' )
			$array['id'] = $res['id'];
		
		$sql = "SELECT nome FROM prodotto p JOIN prodotto_generico pg ON p.id=pg.prodotto_id WHERE p.id=".$prodotto_id;

		$res = mysql_query($sql);

		if ( $res != null and $res != ' ' )
			$array['nome1'] = $res['nome'];

		$sql = "SELECT nome FROM prodotto p JOIN prodotto_generico pg ON p.id=pg.prodotto_id WHERE p.id=".$prodotto_correlato_id;

		$res = mysql_query($sql);

		if ( $res != null and $res != ' ' )
			$array['nome2'] = $res['nome'];

        @mysql_close( $link );
		return $array;
	}

	return false;
}

function getNomiProdottiCorrelato(){
	$ret = 0;

	if ( $link ) {
		
		$sql = "SELECT pc.id, (SELECT nome FROM prodotto WHERE id = pc.prodotto_id) AS nome1, (SELECT nome FROM prodotto WHERE id = pc.prodotto_correlato_id) AS nome2 FROM prodotto_correlato pc ORDER BY id";

		$ret = mysql_query($sql);

        @mysql_close( $link );
		return $ret;
	}

	return false;
}

function buildCheckBoxProdottiCorrelatiById( $istanza_prodotto_id ) {
	$ret = '';
	
    $link = connect();
	if ( $link && isset($istanza_prodotto_id) && $istanza_prodotto_id != "") {
		$sql = "SELECT p.id AS id, pg.nome AS nome, c.nome AS colore, (SELECT count(*) FROM prodotto_correlato pc WHERE pc.prodotto_id=".$istanza_prodotto_id." AND pc.prodotto_correlato_id=p.id) AS count FROM prodotto p JOIN prodotto_generico pg ON p.prodotto_generico_id=pg.id JOIN colore c ON p.colore_id=c.id WHERE pg.attivo=1 AND p.id != ".$istanza_prodotto_id." ORDER BY p.id";

		
		$rets = mysql_query($sql);
		@mysql_close( $link );
		
        while ( $row = mysql_fetch_array( $rets ) ) {

			if ($row['count'] == 1)
				$ret = $ret."<input type='checkbox' name='prodotti_correlati[]' value='".$row['id']."' checked='checked'/>&nbsp;".$row['nome']." - ".$row['colore']."<br />";
			else
    			$ret = $ret."<input type='checkbox' name='prodotti_correlati[]' value='".$row['id']."'/>&nbsp;".$row['nome']." - ".$row['colore']."<br />";
	    }
		
		return $ret;
	}
	
	return $ret;
}

/*_________________________________________________________________*/
//					Gestione Vetrina
/*_________________________________________________________________*/

function getProdottoGenericoByIdVetrina( $prodotto_id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT pg.id, pg.nome, pg.descrizione FROM prodotto p JOIN prodotto_generico pg ON p.prodotto_generico_id = pg.id  WHERE p.id = ".$prodotto_id;
        $res = mysql_query( $sql );
        
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['id'] = $row['id'];
        	$array_ret['nome'] = $row['nome'];
            $array_ret['descrizione'] = $row['descrizione'];
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return "";
}


function getAvatarByIdVetrina( $prodotto_id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT nome, file FROM avatar WHERE prodotto_id=".$prodotto_id;
        $res = mysql_query( $sql );
        
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'] = $row['nome'];
            $array_ret['file'] = $row['file'];
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return "";
}

function getImmagineDettaglioByIdVetrina( $prodotto_id ) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT file FROM immagine_dettaglio WHERE prodotto_id=".$prodotto_id;
        $res = mysql_query( $sql );
        
        if( mysql_num_rows($res) > 0 ){
	        $ret = "";
                $elenco = "";
                $i = 0;
	        while ( $row = mysql_fetch_array( $res ) ) {
	            $ret = $ret."<li><a href='".$row['file']."'>
									<img src='".$row['file']."' alt='' class='thum_gallery'/></a>
							</li>";
                    $elenco[$i]['small'] = $row['file'];
                    $elenco[$i]['big']  = $row['file'];
                    $elenco[$i]['titolo'] = $i;
                    $i++;
	        }
	        @mysql_close( $link );
	        return $elenco;
        } else 
        	return null;
        	return "<p>Anteprime dettagli non disponibili</p>";
    }
    
    return "";
}

function getImmagineRotazioneByIdVetrina($prodotto_id) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT nome, file, posizione FROM immagine_rotazione WHERE prodotto_id=".$prodotto_id;
        $res = mysql_query( $sql );
        
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'.$row['posizione']] = $row['nome'];
            $array_ret['file'.$row['posizione']] = $row['file'];
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return "";
}

function getColoriByIdVetrina($prodotto_generico_id) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT p.id, c.codice, c.nome FROM prodotto p JOIN colore c ON p.colore_id = c.id WHERE prodotto_generico_id =".$prodotto_generico_id;
        $res = mysql_query( $sql );
        
        $ret = "";
        //<a href="#"><img src="vetrina/images_sample/bianco.gif"></a>
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret = $ret."<a href='vetrina.php?id=".$row['id']."'><img src='".$row['codice']."'></a>";
        }
        @mysql_close( $link );
        return $ret;
    }
    
    return "";
}

function getVideoByIdVetrina($prodotto_id) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT nome, file FROM video WHERE prodotto_id=".$prodotto_id;
        $res = mysql_query( $sql );
        
        $array_ret = array();
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'] = $row['nome'];
            $array_ret['file'] = $row['file'];
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return "";
}

function getCorrelatiByIdVetrina($prodotto_id) {
    $link = connect();
	if ( $link )
    {
        $sql = "SELECT file, prodotto_correlato_id FROM prodotto_correlato pc JOIN immagine_rotazione ir ON pc.prodotto_correlato_id = ir.prodotto_id WHERE pc.prodotto_id =".$prodotto_id ." and ir.posizione=1";
        $res = mysql_query( $sql );
        
        if( mysql_num_rows($res) > 0 ){
	        $ret = "";
	        //<a href="#"><img src="vetrina/images_sample/bianco.gif"></a>
	        while ( $row = mysql_fetch_array( $res ) ) {
	            $ret = $ret."<a href='vetrina.php?id=".$row['prodotto_correlato_id']."'><img src='".$row['file']."' width='73px'></a>";
	        }
	        @mysql_close( $link );
	        return $ret;
        } else
        	return "<p>Nessun capo associato</p>";
    }
    
    return "";
}

/*_________________________________________________________________*/
//					Gestione Prodotti
/*_________________________________________________________________*/

function getProdottiElencoByCollezione($id_collezione) {
    $link = connect();
    
    if ( $link )
    {
        $sql = "SELECT pg.nome, ir.file, p.id
				FROM prodotto p
				JOIN prodotto_generico pg ON p.prodotto_generico_id = pg.id
				JOIN immagine_rotazione ir ON p.id = ir.prodotto_id
				WHERE ir.posizione = 1 and pg.collezione_id = ".$id_collezione;
                                
        $res = mysql_query( $sql );
        $ret = "";
        while ( $row = mysql_fetch_array( $res ) ) {
            $ret = $ret." <div class='c06-thumb-box'>
							<a href='vetrina.php?id=".$row['id']."'>
								<img class='c06-thumb' src='".$row['file']."' width=84 height = 84/>".$row['nome'].
							"</a>
						 </div>";
        }
        @mysql_close( $link );
        return $ret;
   	}
    return "";
}


function getCollezioniElenco(){
	
	$link = connect();
	if ( $link )
    {
        $sql = "SELECT distinct c.nome, c.id
				FROM collezione c
				JOIN prodotto_generico pg ON pg.collezione_id = c.id";
        $res = mysql_query( $sql );
        
        $array_ret = array();
        $index = 0;
        $array_ret['num'] = mysql_num_rows($res);
        while ( $row = mysql_fetch_array( $res ) ) {
            $array_ret['nome'.$index] = $row['nome'];
            $array_ret['id'.$index] = $row['id'];
            $index++;
        }
        @mysql_close( $link );
        return $array_ret;
    }
    
    return "";
}

function cercaProdotto($nomeProdotto){
	
	$link = connect();
	if ( $link )
    {
        $sql = "SELECT pg.nome, ir.file, p.id
				FROM prodotto p
				JOIN prodotto_generico pg ON p.prodotto_generico_id = pg.id
				JOIN immagine_rotazione ir ON p.id = ir.prodotto_id
				WHERE ir.posizione = 1 and pg.nome LIKE '%".$nomeProdotto."%'";
                                
        $res = mysql_query( $sql );
        $ret = "";
        if(mysql_num_rows($res)>0){
	        while ( $row = mysql_fetch_array( $res ) ) {
	            $ret = $ret." <div class='c06-thumb-box'>
								<a href='vetrina.php?id=".$row['id']."'>
									<img class='c06-thumb' src='".$row['file']."' width=84 height = 84/>".$row['nome'].
								"</a>
							 </div>";
	        }
        } else 
        	$ret = "<div class='col width_810'><h5>Nessun risultato trovato.</h5></div>";
        @mysql_close( $link );
        return $ret;
    }
    
    return "";
	
}
?>