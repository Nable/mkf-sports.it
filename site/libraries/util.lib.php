<?php
include_once( 'database.lib.php' );

function reindirizza( $url, $tempo = 0 ) 
{
    if ( !headers_sent() && $tempo == 0 ) {
        header('Location:' . $url);
    } elseif( !headers_sent() && $tempo > 0 ) {
        header('Refresh:' . $tempo . ';' . $url);
    }

    echo "<meta http-equiv=\"refresh\" content=\"" . $tempo . ";" . $url . "\">";
}

function getAdminLinkType( $opType ) {
    if ( $opType == ADMIN_OP_TYPE_ADD )
        return "<li class='addlink'>";
    else if ( $opType == ADMIN_OP_TYPE_CHANGE )
        return "<li class='changelink'>";
    else if ( $opType == ADMIN_OP_TYPE_DELETE )
        return "<li class='deletelink'>";
    else
        return "<li class='changelink'>";
}

function getBreadcrumbs( $modulo, $link_modulo, $modello ){
    return "
    <div class='breadcrumbs'>
        <a href='../'>
            Pagina iniziale
        </a>
        &rsaquo; 
        <a href='".$link_modulo."'>".$modulo."</a>
        &rsaquo;
        ".$modello."
    </div>";
}

function getAddLink( $url, $modello ){
    return "<li>
      <a href='".$url."' class='addlink'>
      Aggiungi ".$modello."
    </a>
    </li>";
}

function getPaginator( $table_name, $modello ) {
    $pag = "<p class='paginator'>";
    
    $count = getNumRows( $table_name );

    $pag = $pag.$count." ".$modello;
    
    $pag = $pag. "</p>";
    
    return $pag;
}

function getEstensione($file_name) {
	$estensione = strtolower(substr($file_name, strrpos($file_name, "."), strlen($file_name)-strrpos($file_name, ".")));
	
	return $estensione;
}

?>