<?php
/* Messaggi pannello di admin */
define('ADMIN_LOGIN_FORM_TITLE', 'Pannello di amministrazione - '.SITE_TITLE);
define('ADMIN_CHANGE_PASSWORD_LABEL', 'Cambia la password');
define('ADMIN_LOGOUT_LABEL', 'Esci');

define('LOGIN_ERROR_MSG_USERPSW', 'Errore: username e/o password errati.');

// Messaggi Change Password
define('CHANGE_EMPTY', 'I campi non sono stati tutti valorizzati.');
define('CHANGE_PASS1_PASS2', 'La nuovo password e la password di conferma non coincidono.');
define('CHANGE_SUCCESS', 'Password cambiata con successo.');
define('CHANGE_ERROR', 'Si � verificato un errore imprevisto.');
?>