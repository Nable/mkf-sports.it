<?php
    /*
     * Quando si crea una variabile contenente un url o un path, 
     * ricordarsi di NON inserire lo slash finale!
     */

    /* Configurazione generale sito */
    define('SITE_TITLE', 'mkf-sports.it');

//    VARIABILI per la configurazione del sito in produzione
//    define('ABSOLUTE_URL', 'http://mkf-sports.it/test');

//  VARIABILI per la configurazione del sito in fase di test
    define('ABSOLUTE_URL', 'http://localhost:8080'); 											//Ennio
//    define('ABSOLUTE_URL', 'http://localhost/mkf-sport/');							//Alessandro
//		define('ABSOLUTE_URL', 'http://localhost:8888/mkf-sports.it/site');	//Giacomo	
    
    define('ADMIN_ABSOLUTE_URL', ABSOLUTE_URL.'/admin');
    define('ADMIN_CONTROLLERS_ABSOLUTE_URL', ADMIN_ABSOLUTE_URL.'/controllers');
    define('ADMIN_TEMPLATES_ABSOLUTE_URL', ADMIN_ABSOLUTE_URL.'/templates');
    define('ADMIN_TEMPLATES_CRUD_ABSOLUTE_URL', ADMIN_TEMPLATES_ABSOLUTE_URL.'/crud');
    
    define('CONFIG_ABSOLUTE_URL', ABSOLUTE_URL.'/config');
    define('LIBRARIES_ABSOLUTE_URL', ABSOLUTE_URL.'/libraries');

    define('RESOURCES_PATH', ABSOLUTE_URL.'/resources');
    define('CSS_PATH', RESOURCES_PATH.'/css');
    define('JS_PATH', RESOURCES_PATH.'/js');
    define('ADMIN_IMAGES_PATH', RESOURCES_PATH.'/images/admin');
    define('UPLOAD_IMAGES_PATH', RESOURCES_PATH.'/images/upload');

//  Tipi di operazione CRUD 
    define( 'ADMIN_OP_TYPE_ADD', 'ADD');
    define( 'ADMIN_OP_TYPE_CHANGE', 'CHANGE');
    define( 'ADMIN_OP_TYPE_DELETE', 'DELETE');
    
// DEFINIZIONE MODULI E MODELLI
   define( 'MODULE_ADMIN', 'Admin' );
   define( 'MODULE_ADMIN_MODEL_USERS', 'Utenti' );
   define( 'MODULE_ADMIN_MODEL_USERS_SINGOLARE', 'Utente' );

   define( 'MODULE_MEDIA', 'Media');
   define( 'MODULE_MEDIA_MODEL_AVATARS', 'Avatars');
   define( 'MODULE_MEDIA_MODEL_IMMAGINI_DETTAGLIO', 'Immagini Di Dettaglio');
   define( 'MODULE_MEDIA_MODEL_IMMAGINI_ROTAZIONE', 'Immagini Di Rotazione');
   define( 'MODULE_MEDIA_MODEL_VIDEO', 'Video');
   
   define( 'MODULE_PRODOTTI', 'Prodotti');
   define( 'MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI', 'Prodotti Generici');
   define( 'MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE', 'Prodotto Generico');
   define( 'MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI', 'Istanze Di Prodotti');
   define( 'MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE', 'Istanza Di Prodotto');
   define( 'MODULE_PRODOTTI_MODEL_PRODOTTI_CORRELATI', 'Prodotti Correlati');
   define( 'MODULE_PRODOTTI_MODEL_PRODOTTI_CORRELATI_SINGOLARE', 'Prodotto Correlato');
   
   define( 'MODULE_PROPRIETA', 'Propriet&agrave;');
   define( 'MODULE_PROPRIETA_MODEL_CATEGORIE', 'Categorie');
   define( 'MODULE_PROPRIETA_MODEL_CATEGORIE_SINGOLARE', 'Categoria');

   define( 'MODULE_PROPRIETA_MODEL_CLIENTELE', 'Clientele');
   define( 'MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE', 'Clientela');

   define( 'MODULE_PROPRIETA_MODEL_COLORI', 'Colori');
   define( 'MODULE_PROPRIETA_MODEL_COLORI_SINGOLARE', 'Colore');
   
   define( 'MODULE_PROPRIETA_MODEL_AVATARS', 'Avatars');
   define( 'MODULE_PROPRIETA_MODEL_AVATARS_SINGOLARE', 'Avatar');
   
   define( 'MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE', 'Video');
   
   define( 'MODULE_PROPRIETA_MODEL_IMM_DETTAGLIO', 'Immagini Dettaglio');
   define( 'MODULE_PROPRIETA_MODEL_IMM_DETTAGLIO_SINGOLARE', 'Immagine Dettaglio');
   
   define( 'MODULE_PROPRIETA_MODEL_IMM_ROTAZIONE', 'Immagini Rotazione');
   define( 'MODULE_PROPRIETA_MODEL_IMM_ROTAZIONE_SINGOLARE', 'Immagine Rotazione');
?>