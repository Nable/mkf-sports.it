<?php
include_once('settings.inc');
/* Messaggi pannello di admin */
define('ADMIN_MAIN_TITLE', 'AMMINISTRAZIONE SITO');
define('ADMIN_LOGIN_FORM_TITLE', 'Pannello di amministrazione - '.SITE_TITLE);
define('ADMIN_CHANGE_PASSWORD_LABEL', 'Cambia la password');
define('ADMIN_LOGOUT_LABEL', 'Esci');

define('ADMIN_LOGIN_ERROR_MSG_USERPSW', 'Errore: username e/o password errati.');
define('ADMIN_CHANGE_EMPTY', 'Errore: inserire tutti i campi per completare la modifica.');
define('ADMIN_CHANGE_ERROR', 'Errore: la modifica della password non e\' stata completata');
define('ADMIN_CHANGE_PASS1_PASS2', 'Errore: la nuova password non e\' stata reinserita correttamente.');
define('ADMIN_CHANGE_SUCCESS', 'Modifica della password completata correttamente.');
?>