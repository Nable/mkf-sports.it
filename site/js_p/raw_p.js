
	function show_share() { 
		JQ('#share-box').fadeIn(200) ; 
	}

	function hide_share() { 
		JQ('#share-box').fadeOut(200) ; 
	}
	
	function mainmenu(){
	JQ("#navigation a").removeAttr("title");
	JQ("#navigation ul").css({display: "none"}); // Opera Fix
	JQ("#navigation li").hover(function(){
			JQ(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
			},function(){
			JQ(this).find('ul:first').css({visibility: "hidden"});
			});
	}

	JQ(document).ready(function(JQ) {
		
		mainmenu(); // Navigation
		
		JQ("#article-list").wrapInner(document.createElement("tr"));
		JQ("#article-list").wrapInner(document.createElement("table"));
		JQ("#article-list .article-wrapper").wrap("<td>");
		
		// IE FIXES
		JQ("#navigation li:last-child").addClass("ie-last-child-fix");
		
		// Share box animation
		JQ('.share-button').mouseenter(function() {
			show_share()
		});

		JQ('#share').mouseleave(function() {
			hide_share();
		});
		
		// Share button animation
		JQ('#share-box a, .social-button-holder a').hover(function(){
			JQ(this).stop().animate({ opacity : 0.5 }, 200 );
			},
			function () {
				JQ(this).stop().animate({ opacity : 1 }, 200 );
			}
		);
		
		// Nav menu collapse
		JQ("#expand-button").click(function () {   		
			JQ(this).toggleClass('collapse').parent('nav').find('ul').slideToggle();
		});
		
		// ---------- Toggle Content ---------- //
		JQ(".expanding .expand-button").click(function () {
			JQ(this).toggleClass('close').parent('div').find('.expand').slideToggle('slow');
		});
		
		// Image hover
		JQ('.hover').hover(function(){
			JQ('img', this).stop().animate({ opacity : 0 }, 500 );
			},
			function () {
				JQ('img', this).stop().animate({ opacity : 1 }, 500 );
			}
		);
		
		// Horizontal Portfolio Sorting
		JQ('ul#filter a').click(function() {
			JQ('td').fadeOut(500);
			
			JQ('ul#filter .current').removeClass('current');
			JQ(this).parent().addClass('current');

			var filterVal = this.getAttribute('title') ;
			
			if(filterVal == 'all') {
				JQ('td.hidden').animate({foo: 1}, 499).fadeIn(500);
				JQ('td').fadeIn(500);
				JQ('td.hidden').removeClass('hidden');
			} else {
				JQ('td').each(function() {
					if(!JQ(".article-wrapper", this).hasClass(filterVal)) {
						JQ(this).addClass('hidden');
					} else {
						JQ(this).animate({foo: 1}, 499).fadeIn(500);
						JQ(this).removeClass('hidden');
					}
				});
			}

			return false;

		});
		
		// Grid Portfolio Sorting
		JQ('ul#filter a').click(function() {
			JQ('#portfolio li').fadeOut(500);
			
			JQ('ul#filter .current').removeClass('current');
			JQ(this).parent().addClass('current');

			var filterVal = this.getAttribute('title') ;
			
			if(filterVal == 'all') {
				JQ('#portfolio li.hidden').animate({foo: 1}, 499).fadeIn(500);
				JQ('#portfolio li').fadeIn(500);
				JQ('#portfolio li.hidden').removeClass('hidden');
			} else {
				JQ('#portfolio li').each(function() {
					if(!JQ(this).hasClass(filterVal)) {
						JQ(this).addClass('hidden');
					} else {
						JQ(this).animate({foo: 1}, 499).fadeIn(500);
						JQ(this).removeClass('hidden');
					}
				});
			}

			return false;

		});
		
		
		// CONTACT VALIDATION
		
		// Setup
		JQ('#respond #submit').before('<h3 class="error">Oops!</h3><ul class="error"></ul>')
		JQ('.error').hide();
		
		var hasMessageError = false;
		var hasNameError = false;
		var hasEmailError = false;
		
		if (document.getElementById("comments")) { 
			hasMessageError = true;
		};
		
		if (document.getElementById("contactName")) { 
			hasNameError = true;
		};
		
		if (document.getElementById("email")) { 
			hasEmailError = true;
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?JQ/;
		};
		
		
		// Validation
		JQ("#commentsText").blur(function(){				   				   
			JQ("#comment-error").remove();
			hasMessageError = false;
			
			var messageVal = JQ("#commentsText").val();
			if(messageVal == '') {
				JQ('.error').slideDown('normal');
				JQ('<li id="comment-error">You forgot to include your comment.</li>').appendTo('ul.error');
				hasMessageError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				JQ('.error').hide('normal');
			}
		});
		
		JQ("#contactName").blur(function(){					   				   
			JQ("#author-error").remove();
			hasNameError = false;
			
			var authorVal = JQ("#contactName").val();
			if(authorVal == '') {
				JQ('.error').slideDown('normal');
				JQ('<li id="author-error">You forgot to include your name.</li>').appendTo('ul.error');
				hasNameError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				JQ('.error').hide('normal');
			}
		});
		
		JQ("#email").blur(function(){					   				   
			JQ("#email-error").remove();
			hasEmailError = false;
			
			var emailVal = JQ("#email").val();
			if(emailVal == '') {
				JQ('.error').slideDown('normal');
				JQ('<li id="email-error">You forgot to include your email address.</li>').appendTo('ul.error');
				hasEmailError = true;
			} else if (!emailReg.test(emailVal)) {
				JQ('.error').slideDown('normal');
				JQ('<li id="email-error">The email address you entered is not valid.</li>').appendTo('ul.error');
				hasEmailError = true;
			}
			
			if(hasMessageError == false && hasNameError == false && hasEmailError == false){
				JQ('.error').hide('normal');
			}
		});
		
		//Comment Form Submit
		JQ('form#commentform').submit(function() {
		
			var messageVal = JQ("#commentsText").val();
			var authorVal = JQ("#contactName").val();
			var emailVal = JQ("#email").val();
			
			if (hasMessageError == false && hasNameError == false && hasEmailError == false) {
				
				if ( url.value == 'Website' ){ 
					url.value = '';
				}
				
				window.navigate("#");
				
			} else {
			
				return false;
			
			}	
			
		});
		
		//Contact Form Submit
		JQ('form#contactForm').submit(function() {
		
			var messageVal = JQ("#commentsText").val();
			var authorVal = JQ("#contactName").val();
			var emailVal = JQ("#email").val();
			
			if (hasMessageError == false && hasNameError == false && hasEmailError == false) {

				JQ('form#contactForm #submit').fadeOut('normal', function() {
					JQ(this).parent().append('<img id="loading" src="images/loader.gif" alt="Loading"/>');
				});
				var formInput = JQ(this).serialize();
				JQ.post(JQ(this).attr('action'),formInput, function(data){
					JQ('form#contactForm').slideUp("fast", function() {				   
						JQ(this).before('<h3>Thanks!</h3><p>Your message was successfully sent.</p>');
					});
				});
			}
			
			return false;
			
		});
		
	});	