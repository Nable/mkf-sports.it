<?php 
include_once('config/database.inc');
include_once('config/menu.php');
include_once('config/settings.inc');
include_once('libraries/util.lib.php');
include_once('libraries/database.lib.php'); 
	$id_prodotto = $_GET['id'];
	
	$array_generico = getProdottoGenericoByIdVetrina($id_prodotto);
	$id_generico = $array_generico['id'];
	$nome_prodotto = $array_generico['nome'];
	$descrizione = $array_generico['descrizione'];
	//dall'id arrivo al prodotto generico e recupera il Nome
	
	$array_avatar = getAvatarByIdVetrina($id_prodotto);
	$nome_avatar = $array_avatar['nome'];
	$file_avatar = $array_avatar['file'];
	
	$immagini_dettaglio = getImmagineDettaglioByIdVetrina($id_prodotto);
	
	$array_rotazione = getImmagineRotazioneByIdVetrina($id_prodotto);
	$immagine_rotazione_fronte = $array_rotazione['file1'];
	$immagine_rotazione_retro = $array_rotazione['file2'];
	$immagine_rotazione_indossata = $array_rotazione['file3'];
	
	
	$colori = getColoriByIdVetrina($id_generico);
	
	$array_video = getVideoByIdVetrina($id_prodotto);
	$file_video = $array_video['file'];
	$nome_video = $array_video['nome'];
	
	$correlati = getCorrelatiByIdVetrina($id_prodotto);
?>

<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/home.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:23 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="css/prettyPhoto.html" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	
	<link href="style.css" type="text/css" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/style_vetrina.css">
	<link type="text/css" rel="stylesheet" href="vetrina/css/pannello_prodotti.css">
		
	
	<style>
		#coloriProdotto a {
			margin-left: 2px;
		}
		
		#imageviewDettaglio {
			width:300px;
			height:370px;
			display:none
		}
		
		#imageview {
			width:300px;
			height:370px;
			display: block;
		}
		
		#thumbnails {
			outline: 0px solid #000;
			
		}
	</style>
	<!-- JAVASCRIPTS -->
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
	
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- script src="vetrina/js/cloud-zoom.1.0.2.js"></script -->
	
	<script type='text/javascript' src='vetrina/jwplayer/swfobject.js'></script>
	<script type='text/javascript' src='vetrina/js/jquery.tabify.source.js'></script>
	<script type='text/javascript' src='vetrina/js/jquery.jqzoom-core.js'></script>
	<script type='text/javascript' src='vetrina/js/simplethumbs.js'></script>
	

	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">
	
	function dettaglioZoom(immagine, idProdotto, posizione, contenitore, nascondere) {
			
			console.log(immagine + " " + idProdotto + " " + posizione + " " + contenitore + " " + nascondere);
			
			
			idDiv = "#"+contenitore;
			
			idNascondere = '#immagineviewDettaglio';
			if( nascondere != null && nascondere != undefined)
				idNascondere = "#"+nascondere;
			
			$(idNascondere).hide();
			immagineAttuale = $(idDiv).find('img');				
			immagineAttuale.attr('src', immagine);
			
			linkNuovaImmagine = "dettaglioProdotto.php?id="+idProdotto+"&pos="+posizione;
			
			
			$("#linkDettaglio").attr('href', linkNuovaImmagine);
			
			
			$(idDiv).show();
			
		}
		
		function dettaglio(immagine, contenitore, nascondere) {
			
			idDiv = "#"+contenitore;
			
			idNascondere = '#immagineview';
			if( nascondere != null && nascondere != undefined)
				idNascondere = "#"+nascondere;
				
			$(idNascondere).hide();
			immagineAttuale = $(idDiv).find('img');
			immagineAttuale.attr('src', immagine);				
			$(idDiv).show();
		}
		
	
	$(document).ready(function(){

		$("a[rel^='prettyPhoto']").prettyPhoto({
			theme: 'dark_square'
		});

		$.backstretch("images/background.jpg", {speed: 'slow'});
		
		$('#menuProdotto').tabify();
		
		$('#imageview').find('img').attr({ width: '290', height:'360'});
		$('#imageviewDettaglio').show();
		$('#imageviewDettaglio').find('img').attr({ width: '290', height:'360'});
		$('#imageviewDettaglio').hide();
		
		
	});
		
		
	</script>

<!-- Vetrina -->

<!-- Vetrina -->
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
			<img src="images/logo.png" alt="Website Logo" />
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>
	
	<!-- CONTENT -->
	<div id="content" class="clearfix">
	
	<div class="article-wrapper">
			
			<!-- CONTENT -->
			<article>
				<div id="container">
					<div id="contentProdotto" class="clearfix">
  
					<!-- INIZIO COLONNA 810 -->
					  <div class="col width_810 main">
						<h4 class='nome-capo'><?php echo $nome_prodotto; ?></h4></div>
					
					<!-- FINE COLONNA 810 -->
					
					<!-- INIZIO COLONNA 320 -->
					  
						<div class="col width_320 main" style="height:600px;min-height:600px;">
						    <h4>ANTEPRIMA CAPO</h4>			
							<div class="box">
								
							  <div class="sub-box" id="coloriProdotto">
								<?php echo $colori;?>
							  </div>
							  
							  <div id="immagineProdotto" class="sub-box">
								    
								    <div id="main_view">
									
									<div id="imageviewDettaglio"> 
										<img src="<?php echo $immagine_rotazione_fronte; ?>" alt="example"  /> 
									</div>
									
									<div id="imageview">
										<a id="linkDettaglio" href="dettaglioProdotto.php?id=<?php echo $id_prodotto . "&pos=1" ; ?>"> 
											<img src="<?php echo $immagine_rotazione_fronte; ?>" alt="example"  />
										</a>
									</div>
									 
									<div id="thumbnails">
										
										<?php if( isset( $immagine_rotazione_fronte)) { 
										
											$urlComando = "javascript:dettaglioZoom('".$immagine_rotazione_fronte."',".$id_prodotto.","." 1, 'imageview', 'imageviewDettaglio'); ";   ?>
											
											<a href="<?php echo $urlComando; ?>" class="active">
												<img title="Fronte" src="vetrina/images/fronte.gif" alt="" />
											</a>
										<?php }
											
											if( isset( $immagine_rotazione_retro))  { 
											
											$urlComando = "javascript:dettaglioZoom('".$immagine_rotazione_retro."',".$id_prodotto.","." 2, 'imageview', 'imageviewDettaglio'); ";   ?>
											
											<a href="<?php echo $urlComando; ?>">
												<img title="Retro" src="vetrina/images/retro.gif" alt="" />
											</a>
											
										<?php 	}
											
											if( isset( $immagine_rotazione_indossata)) { 
											
											$urlComando = "javascript:dettaglioZoom('".$immagine_rotazione_indossata."',".$id_prodotto.","." 3, 'imageview', 'imageviewDettaglio'); ";   ?>
											
												<a href="<?php echo $urlComando; ?>">
													<img title="Indossato" src="vetrina/images/indossato.gif" alt="" />
												</a>
										<?php   } ?>
									</div>
									
								    </div> <!-- main_view -->
								    
					
								    <div id="productVideo">
								      <div id='videoProdotto'></div>
								      
								      <?php 
									   
									if($file_video != "") {
										echo"
										      <script type='text/javascript'>
											  var so = new SWFObject('vetrina/jwplayer/player.swf','mpl','300','370','9');
											  so.addParam('allowfullscreen','true');
											  so.addParam('allowscriptaccess','always');
											  so.addParam('wmode','opaque');
											  so.addVariable('author','MFK Sport');
											  so.addVariable('file','".$file_video."')
											  so.addVariable('title','Prodotto');
											  so.addVariable('autostart','true');
											  so.write('videoProdotto');
										      </script>";
									} else 
										echo "<p>Video non disponibile</p>";
								      ?>
								    </div> <!--  productVideo -->
					
							  </div>
							 <div class="clear">
							 </div>
							<div id="divMenuProdotto">
								  <ul id="menuProdotto">
								      <li class="active">
									      <a href="#main_view">
										      <img src="vetrina/images/immagini.png" width="32px" height="32px" style="border: 0px" alt="Immagini" title="Immagini" />
									      </a>
								      </li>
								      <li>
									      <a href="#productVideo">
										      <img src="vetrina/images/video.png" width="32px" height="32px" style="border: 0px" alt="Video" title="Video" />
									      </a>
								      </li>
								  </ul>
							</div>
		  
            </div> 			
            <h4>CAPI ASSOCIATI</h4>
        	<div class="box">
        	  <div class="sub-box">
					<?php echo $correlati; ?>
			  </div>
            </div> 			
        </div> 

<!-- FINE COLONNA 320 -->		
		
<!-- INIZIO COLONNA 260 -->
		
    	<div class="col width_260" style="height:600px;min-height:600px;">
          <h4>ANTEPRIME DETTAGLI</h4>
        	<div class="box">
        	 
		  <div class="sub-box">
			<ul class="thumb">
			  	<?php 
			  	if (isset($immagini_dettaglio)){
				foreach ($immagini_dettaglio as $valore) {
				?>
				<li>
					<a href="javascript:dettaglio('<?php echo $valore['big'] ?>', 'imageviewDettaglio', 'imageview');">
						<img src="<?php echo $valore['small'] ?>" alt="<?php echo $valore['titolo'] ?>" title="<?php echo $valore['titolo'] ?>"/>
					</a>
				</li -->
				
				
							
				<?php	}
				
				} else {
					echo "<p>Anteprime dettagli non disponibili</p>";
				}
				
				//echo $immagini_dettaglio;
				
				?>
			</ul>
			
			
	      </div>
	      
            </div>
        	<div class="box">
		
	
		  <h4>AVATAR</h4>			
        	  <div class="sub-box">
                	<?php if( $file_avatar != "" )  { ?>
				<img src=" <?php echo $file_avatar ?> " width="150px" height="150px">
			<?php	} else { ?>
                 			<p>Avatar non disponibile</p>; 
                		<?php } ?>
                		
		  </div>
            </div> 						
        </div> 						
				
<!-- FINE COLONNA 260 -->

<!-- INIZIO COLONNA 220 -->
		
    	<div class="col width_220 last">
          <h4>DESCRIZIONE</h4>
        	<div class="box">
        	  <div class="sub-box">
                	<?php 
                		if ( $descrizione != "" )
                			echo "<p>".$descrizione."</p>";
                		else
                			echo "<p>Descrizione non disponibile</p>";
                	?>
              </div> 
            </div> 
        </div> 
		
<!-- FINE COLONNA 220 -->				
		
    </div> 
</div>
		<script>
            
        </script>
			</article>
	</div>		
	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/home.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:23 GMT -->
</html>