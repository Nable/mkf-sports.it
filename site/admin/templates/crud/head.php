<?php

echo"<head>
 <title> ".SITE_TITLE." </title>
 <link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_base.php' />
 <link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_forms.php' />
  
 <link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_changelists.css' />
  
 <script type='text/javascript' src='".JS_PATH."/core.js'></script>
 <script type='text/javascript' src='".JS_PATH."/RelatedObjectLookups.js'></script>
 <script type='text/javascript' src='".JS_PATH."/getElementsBySelector.js'></script>
 <script type='text/javascript' src='".JS_PATH."/actions.js'></script>
 <script type='text/javascript' src='".JS_PATH."/SelectBox.js'></script>
 <script type='text/javascript' src='".JS_PATH."/SelectFilter2.js'></script>
 <script type='text/javascript' src='".JS_PATH."/calendar.js'></script>
 <script type='text/javascript' src='".JS_PATH."/DateTimeShortcuts.js'></script>

 <script type='text/javascript' src='".JS_PATH."/tinymce_3_3_8/jscripts/tiny_mce/tiny_mce.js'></script>
 <script type='text/javascript' src='".JS_PATH."/tinymce_3_3_8/textareas.js'></script>
 <script type='text/javascript' src='".RESOURCES_PATH."/jsi18n'></script>
  

 <!--[if lte IE 7]><link rel='stylesheet' type='text/css' href='".CSS_PATH."/ie.css' /><![endif]-->


<meta name='robots' content='NONE,NOARCHIVE' />
</head>";
?>