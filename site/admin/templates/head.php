<?php
include_once('../config/settings.inc');
include_once('../config/messages.inc');
include_once('../config/admin_messages.inc');

echo "<head>
<title> ".SITE_TITLE." </title>
<link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_base.php' />
<link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_dashboard.css' />
<link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_forms.php' />
<link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_style.php' />
<!--[if lte IE 7]><link rel='stylesheet' type='text/css' href='".CSS_PATH."/admin_ie.css' /><![endif]-->

<meta name='robots' content='NONE,NOARCHIVE' />
</head>";
session_start();
?>
