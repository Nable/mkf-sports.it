<div id="content-related">
    <div class="module" id="recent-actions-module">
        <h2>Azioni Recenti</h2>
        <h3>Tutte le azioni dell'admin</h3>
        
        <ul class="actionlist">
        <?php
            include_once('../libraries/database.lib.php');
            // la query ritorna tre campi: TITOLO, TESTO, INSERT_AT
            $resource = get_logs();

            while ( $array = mysql_fetch_array( $resource  ) ) {
                echo getAdminLinkType( $array['op_type']);
                echo "<span class='mini custom_quiet_title'>".$array['titolo']."</span><br />";
                echo "<span class='mini quiet'>".$array['testo']."</span><br />";
                echo "<span class='mini custom_quiet_date'>" .$array['insert_at']."</span><br />";
                echo "</li>";
            }
        ?>
        </ul>
    </div>
</div>