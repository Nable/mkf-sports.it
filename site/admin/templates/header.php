<?php

echo "<div id='header'>
        <div id='branding'>
            <a href=".ADMIN_ABSOLUTE_URL."><h1 id='site-name'>".ADMIN_LOGIN_FORM_TITLE."</h1></a>
        </div>

        <div id='user-tools'>
            Benvenuto/a,
            <strong>".$_SESSION['logged']."</strong>.
            <a href='".ABSOLUTE_URL."/admin/password_change.php'> " .ADMIN_CHANGE_PASSWORD_LABEL. " </a> /
            <a href='".ABSOLUTE_URL."/admin/auth.php?logout=1'>" .ADMIN_LOGOUT_LABEL. "</a>
        </div>
    </div>";
?>
