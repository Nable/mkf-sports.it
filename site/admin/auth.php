<?php
include_once('../config/database.inc');
include_once('../config/admin_messages.inc');
include_once('../config/messages.inc');
include_once('../config/settings.inc');
include_once('../libraries/database.lib.php');
include_once('../libraries/util.lib.php');

if( @$_GET['logout'] == 1 ) //Effettuo il Logout
{
    session_start();
    $_SESSION = array(); //Desetto tutte le variabili di sessione
    session_destroy(); //Distruggo le sessioni 
    reindirizza( ABSOLUTE_URL."/admin/index.php", 0);
    exit();
}

//if( @$_GET['change_pass'] == 1 ) //Richiesta di cambio password
//{
//  session_start();
//  if( !IsSet($_SESSION['logged']) )
//      reindirizza( ABSOLUTE_URL."/admin/index.php", 0);
//    else  
//      reindirizza( ABSOLUTE_URL."/admin/password_change.php", 0);
//    exit();
//}

if( @$_GET['change_pass'] == 2 ) //Effettuo il cambio della password
{
    session_start();
    $_SESSION['change_msg_error'] = "";
    $_SESSION['change_msg'] = "";
    
    if ( !IsSet($_POST['old_password']) || !IsSet($_POST['new_password1']) || !IsSet($_POST['new_password2']) )
    {
        $_SESSION['change_msg_error'] = ADMIN_CHANGE_EMPTY;
        reindirizza( ABSOLUTE_URL."/admin/password_change.php" );
        exit();
    }
    elseif ($_POST['new_password1'] != $_POST['new_password2']) 
    {
        $_SESSION['change_msg_error'] = ADMIN_CHANGE_PASS1_PASS2;
        reindirizza( ABSOLUTE_URL."/admin/password_change.php");
        exit();
    }
    else
    {
        if(($_POST['new_password1']=="" || $_POST['new_password2'] == ""))
        {
            $_SESSION['change_msg_error'] = ADMIN_CHANGE_EMPTY;
            reindirizza( ABSOLUTE_URL."/admin/password_change.php");
            exit();
        }
        else
        {
            $flag_salvataggio = modifica_password($_POST['old_password'], $_POST['new_password1']);
            
            if($flag_salvataggio == PASS_ERR)
                $_SESSION['change_ms_errorg'] = ADMIN_CHANGE_ERROR;   
            else 
                $_SESSION['change_msg'] = ADMIN_CHANGE_SUCCESS;
            reindirizza( ABSOLUTE_URL."/admin/password_change.php");
            exit;
        }
    }
}

// se l'utente non e' loggato
if( !IsSet($_SESSION['logged']) )
{
    $_SESSION['login_msg'] = "";
    if ( !IsSet($_POST['username']) || !IsSet($_POST['password']) )
    {
        reindirizza( ABSOLUTE_URL."/admin/index.php" );
        exit();
    }

    session_start();
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ( ( $username == "" || $password == "" ) )
    {
        $_SESSION['login_msg'] = LOGIN_ERROR_MSG_USERPSW;
        reindirizza( ABSOLUTE_URL."/admin/index.php" );
        exit();
    }
    else
    {
        // login OK
        if ( query_login($username, $password, ADMIN_USER_TABLE) )
        {
            $_SESSION['logged'] = $username;
            reindirizza( ABSOLUTE_URL."/admin/reserved.php" );
            exit();
        } else { // login KO
            $_SESSION['login_msg'] = LOGIN_ERROR_MSG_USERPSW;
            reindirizza( ABSOLUTE_URL."/admin/index.php" );
            exit();
        }
    }   
}
else if ( IsSet( $_SESSION['logged'] ) )
{
    reindirizza( ABSOLUTE_URL."/admin/reserved.php" );
    exit();
}
?>