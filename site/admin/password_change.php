<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="it-it" xml:lang="it-it" >

<?php include_once('./templates/head.php'); 
?>

<body class="dashboard">

<!-- Container -->

<div id="container">
    
    <?php include_once('./templates/header.php');?>

    <!-- Content -->
    <div id="content" class="colMS">

        <?php
            // le variabili son state caricate nell'header.php
            echo "<h1>".ADMIN_MAIN_TITLE."</h1>";
        ?>
        
        <div id="content-main">
            <?php
            include_once('../libraries/util.lib.php'); 
            session_start();
            if ( isset ( $_SESSION ['logged'] )) { 
                echo "<form action='".ABSOLUTE_URL."/admin/auth.php?change_pass=2' method='post' id='login-form'>";
                echo "<div>
                    <h1>Cambio password</h1>
                    <p>Inserisci la password attuale, per ragioni di sicurezza, e poi la nuova password due volte, per verificare di averla scritta correttamente.</p>
                    <fieldset class='module aligned wide'>
                        <div class='form-row'>
                            <label for='id_old_password' class='required'>Password attuale:</label>
                            <input type='password' name='old_password' id='id_old_password' />
                        </div>
                        <div class='form-row'>
                            <label for='id_new_password1' class='required'>Nuova password:</label>
                            <input type='password' name='new_password1' id='id_new_password1' />
                        </div>
                        <div class='form-row'>
                            <label for='id_new_password2' class='required'>Password (di nuovo):</label>
                            <input type='password' name='new_password2' id='id_new_password2' />
                        </div>";
                        
                        if($_SESSION['change_msg_error'] != "")
                        {
                        echo "<ul class='errorlist'>
                                <li>".$_SESSION['change_msg_error']."</li>
                            </ul>";
                        }
                        if($_SESSION['change_msg'] != "")
                        {
                        echo "<ul class='successlist'>
                                <li>".$_SESSION['change_msg']."</li>
                            </ul>";
                        }
                    echo"</fieldset>
                    <div class='submit-row'>
                        <input type='submit' value='Modifica la mia password' class='default' />
                    </div>
                    <script type='text/javascript'>document.getElementById('id_old_password').focus();</script>
                </div>
                </form>";
            }
            else
            {
                reindirizza( ABSOLUTE_URL."/admin/index.php", 0 );
                exit ();
            }
        ?>  
        </div>
        
        
        <br class="clear" />

    </div>
    <!-- END Content -->

    <?php include_once('./templates/footer.php'); ?>
</div>
<!-- END Container -->

</body>
</html>