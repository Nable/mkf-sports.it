<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset($_POST['attivo']))
   $_POST['attivo'] = 'off';

if ( !isset( $_POST['nome'] ) or !isset( $_POST['descrizione'] ) or !isset( $_POST['attivo'] ) or !isset( $_POST['categoria'] ) or !isset( $_POST['collezione'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['nome'] == "" or $_POST['categoria'] == "" or $_POST['collezione'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( addProdottoGenerico( $_POST['nome'], $_POST['descrizione'], $_POST['attivo'], $_POST['categoria'], $_POST['collezione'] ) ) {
    $_SESSION['added'] = 1;
    insert_log( $_POST['nome'], PRODOTTO_GENERICO_TABLE, ADMIN_OP_TYPE_ADD );
}
else
    $_SESSION['added'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_prodotti_generici.php' );
?>