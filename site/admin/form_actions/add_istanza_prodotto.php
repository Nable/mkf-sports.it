<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset($_POST['attivo']))
   $_POST['attivo'] = 'off';

if ( !isset( $_POST['prodotto_generico_id'] ) or !isset( $_POST['colore_id'] ) or !isset( $_POST['attivo'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['prodotto_generico_id'] == "" or $_POST['colore_id'] == "" or $_POST['attivo'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( addIstanzaProdotto( $_POST['prodotto_generico_id'], $_POST['colore_id'], $_POST['attivo'], $_POST['prodotti_correlati'] ) ) {
    $_SESSION['added'] = 1;
    insert_log( $_POST['prodotto_generico_id'], PRODOTTO_TABLE, ADMIN_OP_TYPE_ADD );
}
else
    $_SESSION['added'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_istanze_prodotti.php' );
?>