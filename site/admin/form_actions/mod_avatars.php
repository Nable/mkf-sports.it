<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

//TODO
if ($_POST['prodotto'] == "")
	$_POST['prodotto'] =1;

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['nome'] ) or !isset( $_POST['prodotto'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['nome'] == "" or $_POST['prodotto'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}


if($_FILES['image']['size'] > 0){	
	//controlliamo se ci sono stati errori durante l'upload
	if ($_FILES['image']["error"] > 0){
		//echo "Codice Errore: " . $_FILES["image"]["error"]."";
		$_SESSION['changed']=0;
		reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_avatars.php');
		exit();
	}
	$to_upload = UPLOAD_IMAGES_PATH."/avatar/".$_FILES["image"]["name"];
}
else {
	$to_upload = $_POST['old_file'];
}

if (updateAvatar( $_POST['id_avatar'], $_POST['nome'], $_POST['prodotto'], $to_upload ) ) {
	if($_FILES['image']['size'] > 0){
		@unlink("../../resources/images/upload/avatar/".$_POST['old_file']);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../../resources/images/upload/avatar/" . $_FILES["image"]["name"]);
	}
	$_SESSION['changed']=1;
    insert_log( $_POST['nome'], AVATAR_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed']=0;
reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_avatars.php');
?>