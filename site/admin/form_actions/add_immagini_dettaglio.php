<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['nome'] ) or !isset( $_POST['prodotto'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['nome'] == "" or $_POST['prodotto'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

//controlliamo se ci sono stati errori durante l'upload
if ($_FILES['image']["error"] > 0){
	$_SESSION['added'] = 0;
	reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_immagini_dettaglio.php');
	exit();
}

move_uploaded_file($_FILES["image"]["tmp_name"], "../../resources/images/upload/dettaglio/" . $_FILES["image"]["name"]);

if (addImmDettaglio( $_POST['nome'], $_POST['prodotto'], UPLOAD_IMAGES_PATH."/dettaglio/".$_FILES["image"]["name"] )) {
	$_SESSION['added'] = 1;
    insert_log( $_POST['nome'], IMM_DETTEGLIO_TABLE, ADMIN_OP_TYPE_ADD );
} else
    $_SESSION['added'] = 0;
reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_immagini_dettaglio.php');

?>