<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['fields_to_del'] ) or count($_POST['fields_to_del']) == "0" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}



$cancellati = 0;
$scartati = 0;
foreach( $_POST['fields_to_del']  as  $avatar ) {
    $file = getAvartFileById($avatar);
    $file = str_replace ( UPLOAD_IMAGES_PATH."/avatar/" , "" , $file);
    
    if ( delAvatar( $avatar ) ) {
		@unlink("../../resources/images/upload/avatar/".$file);
        $_SESSION['deleted'] = 1;
        $cancellati++;
    }
    else {
        $_SESSION['deleted'] = 0;
        $scartati++;
    }
}

$_SESSION['cancellati'] = $cancellati;
$_SESSION['scartati'] = $scartati;
reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_avatars.php');
?>
