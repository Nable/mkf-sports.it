<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['nome'] )) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['nome'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

//controlliamo se ci sono stati errori durante l'upload
if ($_FILES['image']["error"] > 0){
	$_SESSION['added'] = 0;
	reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_colori.php');
	exit();
}

move_uploaded_file($_FILES["image"]["tmp_name"], "../../resources/images/upload/colore/" . $_FILES["image"]["name"]);

if ( addColore( $_POST['nome'], UPLOAD_IMAGES_PATH."/colore/".$_FILES["image"]["name"] ) == true ){
    $_SESSION['added'] = 1;	
    insert_log( $_POST['nome'], COLORE_TABLE, ADMIN_OP_TYPE_ADD );
} else
    $_SESSION['added'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_colori.php');
?>