<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['colore_id'] ) or !isset( $_POST['nome'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['colore_id'] == "" or $_POST['nome'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}


if($_FILES['image']['size'] > 0){	
	//controlliamo se ci sono stati errori durante l'upload
	if ($_FILES['image']["error"] > 0){
		//echo "Codice Errore: " . $_FILES["image"]["error"]."";
		$_SESSION['changed']=0;
		reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_colori.php');
		exit();
	}
	$to_upload = UPLOAD_IMAGES_PATH."/colore/".$_FILES["image"]["name"];
}
else {
	$to_upload = UPLOAD_IMAGES_PATH."/colore/".$_POST['old_file'];
}

if (updateColore( $_POST['colore_id'], $_POST['nome'], $to_upload ) ){
	if($_FILES['image']['size'] > 0){
		@unlink("../../resources/images/upload/colore/".$_POST['old_file']);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../../resources/images/upload/colore/" . $_FILES["image"]["name"]);
	}
	$_SESSION['changed'] = 1;
    insert_log( $_POST['nome'], COLORE_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_colori.php?op=');
?>