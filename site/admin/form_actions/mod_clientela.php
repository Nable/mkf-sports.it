<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['clientela_id'] ) or !isset( $_POST['titolo'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['clientela_id'] == "" or $_POST['titolo'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( updateClientela( $_POST['clientela_id'], $_POST['titolo'] ) )
    $_SESSION['changed'] = 1;
else
    $_SESSION['changed'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_clientele.php' );
?>