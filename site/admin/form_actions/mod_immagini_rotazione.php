<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

//TODO
if ($_POST['prodotto'] == "")
	$_POST['prodotto'] =1;

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['nome'] ) or !isset( $_POST['prodotto'] ) or !isset( $_POST['posizione'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['nome'] == "" or $_POST['prodotto'] == "" or $_POST['posizione'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if($_FILES['image']['size'] > 0){	
	//controlliamo se ci sono stati errori durante l'upload
	if ($_FILES['image']["error"] > 0){
		//echo "Codice Errore: " . $_FILES["image"]["error"]."";
		$_SESSION['changed']=0;
		reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_immagini_rotazione.php');
		exit();
	}
	$to_upload = UPLOAD_IMAGES_PATH."/rotazione/".$_FILES["image"]["name"];
}
else {
	$to_upload = $_POST['old_file'];
}

if (updateImmRotazione( $_POST['id_avatar'], $_POST['nome'], $_POST['prodotto'], $to_upload, $_POST['posizione'] ) ) {
	if($_FILES['image']['size'] > 0){
		$estensione = getEstensione($_POST['old_file']);
		@unlink("../../resources/images/upload/rotazione/".$_POST['old_file']);
		@unlink("../../resources/images/upload/rotazione/".$_POST['old_file'].".resized".$estensione);
		move_uploaded_file($_FILES["image"]["tmp_name"], "../../resources/images/upload/rotazione/" . $_FILES["image"]["name"]);
		
		// Ottengo le informazioni sull'immagine originale
		list($width, $height, $type, $attr) = getimagesize("../../resources/images/upload/rotazione/" . $_FILES["image"]["name"]);
		
		// Ottengo l'estensione del file
		$estensione = getEstensione($_FILES["image"]["name"]);
		
		// Creo la versione dell'immagine ridotta ad 800px (thumbnail)
		/*$thumb = imagecreatetruecolor(1024, 768);
		$source = imagecreatefromjpeg("../../resources/images/upload/rotazione/" . $_FILES["image"]["name"]);
		imagecopyresized($thumb, $source, 0, 0, 0, 0, 1024, 768, $width, $height); */
		
		
		if( round($width / $height) == 1 ) {
		    $widthResized = 800;
		    $heightResized = 600;
		} else {
		    $widthResized = 1024;
		    $heightResized = 768;
		}
		
		//$thumb = imagecreatetruecolor(1024, 768);
		$thumb = imagecreatetruecolor($widthResized, $heightResized);
		$source = imagecreatefromjpeg("../../resources/images/upload/rotazione/" . $_FILES["image"]["name"]);
		//imagecopyresized($thumb, $source, 0, 0, 0, 0, 1024, 768, $width, $height);
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $widthResized, $heightResized, $width, $height);
		
		// Salvo l'immagine ridimensionata ---settare il nome corretto
		imagejpeg($thumb, "../../resources/images/upload/rotazione/".$_FILES["image"]["name"] . ".resized".$estensione, 75);
		//
	}
	$_SESSION['changed']=1;
    insert_log( $_POST['nome'], IMM_ROTAZIONE_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed']=0;
reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_immagini_rotazione.php');
?>