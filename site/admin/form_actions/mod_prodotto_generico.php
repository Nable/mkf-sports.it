<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset($_POST['attivo']))
   $_POST['attivo'] = 'off';

if ( !isset( $_POST['prodotto_generico_id'] ) or !isset( $_POST['nome'] ) or !isset( $_POST['descrizione'] ) or !isset( $_POST['attivo'] ) or !isset( $_POST['categoria'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['prodotto_generico_id'] == "" or $_POST['nome'] == "" or $_POST['categoria'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( updateProdottoGenerico( $_POST['prodotto_generico_id'], $_POST['nome'], $_POST['descrizione'], $_POST['attivo'], $_POST['categoria'] ) ) {
	$_SESSION['changed'] = 1;
    insert_log( $_POST['titolo'], PRODOTTO_GENERICO_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_prodotti_generici.php' );
?>