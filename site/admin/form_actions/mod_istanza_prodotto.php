<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset($_POST['attivo']))
   $_POST['attivo'] = 'off';

if ( !isset( $_POST['istanza_prodotto_id']) or !isset($_POST['prodotto_generico_id'] ) or !isset( $_POST['attivo'] ) or !isset( $_POST['colore_id'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['istanza_prodotto_id'] == "" or $_POST['prodotto_generico_id'] == "" or $_POST['attivo'] == "" or $_POST['colore_id'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( updateIstanzaProdotto( $_POST['istanza_prodotto_id'], $_POST['prodotto_generico_id'], $_POST['colore_id'], $_POST['attivo'], $_POST['prodotti_correlati'] ) ) {
	$_SESSION['changed'] = 1;
    insert_log( $_POST['istanza_prodotto_id'], PRODOTTO_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_istanze_prodotti.php' );
?>