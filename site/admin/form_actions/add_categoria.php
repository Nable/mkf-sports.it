<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['titolo'] ) or !isset( $_POST['clientela'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['titolo'] == "" or $_POST['clientela'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( addCategoria( $_POST['titolo'], $_POST['clientela'] ) ) {
    $_SESSION['added'] = 1;
    insert_log( $_POST['titolo'], CATEGORIA_TABLE, ADMIN_OP_TYPE_ADD );
}
else
    $_SESSION['added'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_categorie.php' );
?>