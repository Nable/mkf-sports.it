<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();

if ( !isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( !isset( $_POST['username_id'] ) or !isset( $_POST['username'] ) or !isset( $_POST['password1'] ) or !isset( $_POST['password2'] ) ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['username_id'] == "" or $_POST['username'] == "" or $_POST['password1'] == "" or $_POST['password2'] == "" ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if ( $_POST['password1'] != $_POST['password2'] ) {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}

if (updateUtente( $_POST['username_id'], $_POST['nome'], $_POST['cognome'], $_POST['username'], $_POST['email'], $_POST['password1'] ) ){
	$_SESSION['changed'] = 1;
    insert_log( $_POST['username'], UTENTE_TABLE, ADMIN_OP_TYPE_CHANGE );
} else
    $_SESSION['changed'] = 0;

reindirizza( ADMIN_ABSOLUTE_URL.'/controllers/admin_utenti.php?op=');
?>