<?php
// Pagina di login al pannello di amministrazione.
include_once ('../config/settings.inc');
include_once ('../config/messages.inc');
include_once ('../libraries/util.lib.php');

session_start ();

if ( isset ( $_SESSION ['logged'] ) ) {
    reindirizza( ABSOLUTE_URL."/admin/reserved.php", 0 );
    exit ();
}

echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>";
echo "<html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";
echo "<head>";
echo "<title> " . SITE_TITLE . " </title>";
echo "<link rel='stylesheet' type='text/css' href='" . CSS_PATH . "/admin_base.php' />";
echo "<link rel='stylesheet' type='text/css' href='" . CSS_PATH . "/admin_login.css' />";
echo "<!--[if lte IE 7]><link rel='stylesheet' type='text/css' href='" . CSS_PATH . "/admin_ie.css' /><![endif]--> ";

echo "<meta name='robots' content='NONE,NOARCHIVE' />";
echo "</head>";

echo "<body class='login'>";

//<!-- Container -->
echo "<div id='container'>";

//    <!-- Header -->
echo "<div id='header'>";
echo "<div id='branding'>";
echo "<h1 id='site-name'> " . ADMIN_LOGIN_FORM_TITLE . "</h1>";
echo "</div>";
echo "</div>";
//    <!-- END Header -->

//    <!-- Content -->
echo "<div id='content' class='colM'>";
echo "<div id='content-main'>";

echo "<form action='" . ABSOLUTE_URL . "/admin/auth.php' method='post' id='login-form'>";
echo "<div class='form-row'>";
echo "<label for='id_username'>Nome utente:</label> <input type='text' name='username' id='id_username' />";
echo "</div>";
echo "<div class='form-row'>";
echo "<label for='id_password'>Password:</label> <input type='password' name='password' id='id_password' />";
//echo "<input type='hidden' name='this_is_the_login_form' value='1' />";
echo "</div>";

if ( ( IsSet($_SESSION['login_msg']) ) &&  ( $_SESSION['login_msg'] != "" ) )
{
    echo "<ul class='errorlist'>
            <li>".$_SESSION['login_msg']."</li>
        </ul>";
}

echo "<div class='submit-row'>";
echo "<label>&nbsp;</label><input type='submit' value='Accedi' />";
echo "</div>";
echo "</form>";
echo "<script type='text/javascript'>document.getElementById('id_username').focus()</script>";
echo "</div>";

echo "<br class='clear' />";
echo "</div>";
//    <!-- END Content -->

echo "<div id='footer'></div>";
echo "</div>";
//<!-- END Container -->

echo "</body>";
echo "</html>";
?>