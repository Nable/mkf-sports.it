<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();
if ( isset ( $_SESSION ['logged'] ) ) {
    if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_ADD ) ) {
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php", MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/add_prodotto_generico.php' method='post' id='prodotto_generico_form'>
                <div>
                    <fieldset class='module aligned'>
                    <div class='form-row nome'>
                        <div>
                            <label for='id_nome' class='required'>Nome:</label><input id='id_nome' type='text' class='vTextField' name='nome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row descrizione'>
                        <div>
                            <label for='id_descrizione' class='required'>Descrizione:</label>
							<textarea id='id_descrizione' rows='10' cols='40' name='descrizione' class='vLargeTextField'></textarea>
                        </div>
                    </div>
                    <div class='form-row attivo'>
                        <div>
                            <label for='id_attivo' class='required'>Attivo</label><input type='checkbox' name='attivo' id='id_attivo' /><label for='id_attivo' class='vCheckboxLabel'>Attivo</label>
                        </div>
                    </div>
                    <div class='form-row categoria'>
                        <div>
                            <label for='id_categoria'>Categoria:</label><select name='categoria' id='id_categoria'>
                            <option value='' selected='selected'>---------</option>
                            ".getCategorieOptions()."
                            </select>
                            <a href='./admin_categorie.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_categoria' onclick='return showAddAnotherPopup(this);'> <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/></a>

                            <p class='help'>Dopo aver inserito una nuova categoria, per favore aggiorna la pagina.</p>
                        </div>
                    </div>
                    <div class='form-row collezione'>
                        <div>
                            <label for='id_collezione'>Collezione:</label><select name='collezione' id='id_collezione'>
                            <option value='' selected='selected'>---------</option>
                            ".getCollezioneOptions()."
                            </select>
                       </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>

           <script type='text/javascript'>document.getElementById('id_nome').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_CHANGE ) and ( isset($_GET['id'] ) ) ) {
        
        $id = $_GET['id'];
        $prodotto = getProdottoGenericoById( $id );
		$nome = $prodotto['nome'];
		$descrizione = $prodotto['descrizione'];
		$attivo = $prodotto['attivo'];

		$checked = "";
		if ( $attivo == true )
		    $checked = "checked";
		$categoria_id = $prodotto['categoria_id'];
		$collezione_id = $prodotto['collezione_id'];
		
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php", MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/mod_prodotto_generico.php' method='post' id='prodotto_generico_form'>
                <div>
                    <fieldset class='module aligned'>
                    <div class='form-row nome'>
                        <div>
                            <label for='id_nome' class='required'>Nome:</label><input id='id_nome' type='text' class='vTextField' name='nome' value='".$nome."' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row descrizione'>
                        <div>
                            <label for='id_descrizione' class='required'>Descrizione:</label>
							<textarea id='id_descrizione' rows='10' cols='40' name='descrizione' class='vLargeTextField'>".$descrizione."</textarea>
                        </div>
                    </div>
                    <div class='form-row attivo'>
                        <div>
                            <label for='id_attivo' class='required'>Attivo</label><input type='checkbox' name='attivo' id='id_attivo' checked='".$checked."' /><label for='id_attivo' class='vCheckboxLabel'>Attivo</label>
                        </div>
                    </div>
                    <div class='form-row categoria'>
                        <div>
                            <label for='id_categoria'>Categoria:</label><select name='categoria' id='id_categoria'>"
                            .getCategoriaSelectedById( $categoria_id )."</select>
                            <a href='./admin_categorie.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_categoria' onclick='return showAddAnotherPopup(this);'> 
                                <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/>
                            </a>
                            <p class='help'>Dopo aver inserito una nuova categoria, per favore aggiorna la pagina.</p>
                        </div>
                    </div>
                    <div class='form-row collezione'>
                        <div>
                        	<label for='id_collezione'>Collezione:</label><select name='collezione' id='id_collezione'>"
                            .getCollezioneSelectedById($collezione_id)."</select>
                       </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>";

          echo "<input type='hidden' name='prodotto_generico_id' value='".$id."' />";
          
          echo "<script type='text/javascript'>document.getElementById('id_title').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else {
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once( '../templates/crud/head.php' );
    
    echo "<body class='change-list'>";

    include('../templates/header.php');
    
    echo "<!-- Container -->
    <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php", MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE );

    if ( $_SESSION['added'] === 1) {
        echo "<ul class='messagelist'><li>Prodotto generico aggiunto con successo.</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['added'] === 0) {
	       echo "<ul class='errorlist'><li>Impossibile aggiungere il prodotto generico.</li></ul>";
	       unset($_SESSION['added']);
	} else if ( $_SESSION['deleted'] === 1) {
        echo "<ul class='messagelist'><li>Prodotto/i generico/i cancellato/i con successo</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['deleted'] === 0) {
        echo "<ul class='errorlist'><li>C'&egrave; qualche problema in fase di cancellazione. Eliminati ".$_SESSION['cancellati']." prodotti generici su ".$_SESSION['cancellati']+$_SESSION['scartati']." selezionati.</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['changed'] === 1) {
        echo "<ul class='messagelist'><li>Prodotto generico modificato con successo.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['changed'] === 0) {
	    echo "<ul class='messagelist'><li>Impossibile modificare il prodotto generico.</li></ul>";
	    unset($_SESSION['changed']);
    }
    
    echo "<!-- Content -->
        <div id='content' class='flex'>
            <h1>Scegli Prodotto Generico da modificare</h1>
            <div id='content-main'>
                <ul class='object-tools'>"
                . getAddLink( ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_ADD, MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI_SINGOLARE ).
                "</ul>

          <div class='module' id='changelist'>          
              <form action='../form_actions/del_prodotti_generici.php' method='post'>
                  <div class='actions'>
                      <label>Azione: <select name='action'>
                          <option value='' selected='selected'>---------</option>
                          <option value='delete_selected'>Cancella Prodotto/i generico/i selezionato/i</option>
                      </select>
                      </label>
                      <button type='submit' class='button' title='Esegui l'azione selezionata' name='index' value='0'>Vai</button>
                  </div>
                <table cellspacing='0'>
                    <thead>
                        <tr>
                            <th>
                                <input type='checkbox' id='action-toggle' />
                            </th>
                            <th>
                                Nome
                            </th>
                            <th>
                                Attivo
                            </th>
                            <th>
                                Categoria
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    "
                    .getProdottoGenericoECategoria().
                    "
                    </tbody>
                </table>"
                . getPaginator( PRODOTTO_GENERICO_TABLE, MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI ) .
            "</form>
          </div> <!-- end class module -->
        </div> <!-- end class content-main -->
        <br class='clear' />
    </div>
    <!-- END Content -->";
    
    include_once('../templates/footer.php');
    
   echo "</div>
   <!-- END Container -->
    </body>
    </html>";
    }
} else {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}
?>