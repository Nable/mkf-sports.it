<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');
session_start();

if ( isset ( $_SESSION ['logged'] ) ) {
    if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_ADD ) ) {

        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php", MODULE_ADMIN_MODEL_USERS_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_ADMIN_MODEL_USERS_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/add_utente.php' method='post' id='colore_form'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row username'>
                        <div>
                            <label for='id_username' class='required'>Username:</label><input id='id_username' type='text' class='vTextField' name='username' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row nome'>
                        <div>
                            <label for='id_nome' class='required'>Nome:</label><input id='id_nome' type='text' class='vTextField' name='nome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row cognome'>
                        <div>
                            <label for='id_cognome' class='required'>Cognome:</label><input id='id_cognome' type='text' class='vTextField' name='cognome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row email'>
                        <div>
                            <label for='id_email' class='required'>Email:</label><input id='id_email' type='text' class='vTextField' name='email' value='' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row password1'>
                        <div>
                            <label for='id_password1' class='required'>Password:</label><input id='id_password1' type='password' class='vTextField' name='password1' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row password2'>
                        <div>
                            <label for='id_password2' class='required'>Ripeti Password:</label><input id='id_password2' type='password' class='vTextField' name='password2' maxlength='255' />
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>

           <script type='text/javascript'>document.getElementById('id_username').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_CHANGE ) and ( isset($_GET['id'] ) ) ) {
        
        $id = $_GET['id'];
        $utente = getUtenteById( $id );
        $nome = $utente['nome'];
        $cognome = $utente['cognome'];
        $email = $utente['email'];
        $username = $utente['username'];
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php", MODULE_ADMIN_MODEL_USERS_SINGOLARE );

        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_ADMIN_MODEL_USERS_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/mod_utente.php' method='post' id='colore_form'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row username'>
                        <div>
                            <label for='id_username' class='required'>Username:</label><input id='id_username' type='text' class='vTextField' value=".$username." name='username' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row nome'>
                        <div>
                            <label for='id_nome' class='required'>Nome:</label><input id='id_nome' type='text' class='vTextField' value=".$nome." name='nome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row cognome'>
                        <div>
                            <label for='id_cognome' class='required'>Cognome:</label><input id='id_cognome' type='text' class='vTextField' value=".$cognome." name='cognome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row email'>
                        <div>
                            <label for='id_email' class='required'>Email:</label><input id='id_email' type='text' class='vTextField' value=".$email." name='email' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row password1'>
                        <div>
                            <label for='id_password1' class='required'>Password:</label><input id='id_password1' type='password' class='vTextField' name='password1' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row password2'>
                        <div>
                            <label for='id_password2' class='required'>Ripeti Password:</label><input id='id_password2' type='password' class='vTextField' name='password2' maxlength='255' />
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>";
                
          echo "<input type='hidden' name='username_id' value='".$id."' />";

          echo "<script type='text/javascript'>document.getElementById('id_nome').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else {
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once( '../templates/crud/head.php' );
    
    echo "<body class='change-list'>";

    include('../templates/header.php');
    
    echo "<!-- Container -->
    <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php", MODULE_ADMIN_MODEL_USERS_SINGOLARE );

    if ( $_SESSION['added'] === 1) {
        echo "<ul class='messagelist'><li>Utente aggiunto con successo</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['added'] === 0) {
	    echo "<ul class='errorlist'><li>Impossibile aggiungere l'utente.</li></ul>";
	    unset($_SESSION['added']);
	} else if ( $_SESSION['changed'] === 1) {
        echo "<ul class='messagelist'><li>Utente modificato con successo.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['changed'] === 0) {
	    echo "<ul class='errorlist'><li>Impossibile modificare l'utente.</li></ul>";
	    unset($_SESSION['changed']);
	} if ( $_SESSION['deleted'] === 1) {
        echo "<ul class='messagelist'><li>Utente/i cancellato/i con successo</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['deleted'] === 0) {
        echo "<ul class='errorlist'><li>C'&egrave; qualche problema in fase di cancellazione. Eliminati ".$_SESSION['cancellati']." utenti su ".$_SESSION['cancellati']+$_SESSION['scartati']." selezionati.</li></ul>";
        unset($_SESSION['deleted']);
    }
    
    echo "<!-- Content -->
        <div id='content' class='flex'>
            <h1>Scegli Utente da modificare</h1>
            <div id='content-main'>
                <ul class='object-tools'>"
                . getAddLink( ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php?op=".ADMIN_OP_TYPE_ADD, MODULE_ADMIN_MODEL_USERS_SINGOLARE ).
                "</ul>

          <div class='module' id='changelist'>
              <form action='../form_actions/del_utenti.php' method='post'>
                  <div class='actions'>
                      <label>Azione: <select name='action'>
                          <option value='' selected='selected'>---------</option>
                          <option value='delete_selected'>Cancella Utente/i selezionati/e</option>
                      </select>
                      </label>
                      <button type='submit' class='button' title='Esegui l'azione selezionata' name='index' value='0'>Vai</button>
                  </div>
                <table cellspacing='0'>
                    <thead>
                        <tr>
                            <th>
                                <input type='checkbox' id='action-toggle' />
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Nome
                            </th>
                            <th>
                                Cognome
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Ultimo accesso
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    "
                    .getUtente().
                    "
                    </tbody>
                </table>"
                . getPaginator( UTENTE_TABLE, MODULE_PROPRIETA_MODEL_COLORI ) .
            "</form>
          </div> <!-- end class module -->
        </div> <!-- end class content-main -->
        <br class='clear' />
    </div>
    <!-- END Content -->";
    
    include_once('../templates/footer.php');
    
   echo "</div>
   <!-- END Container -->
    </body>
    </html>";
    }
} else {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}
?>