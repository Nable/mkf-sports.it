<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();
if ( isset ( $_SESSION ['logged'] ) ) {
    if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_ADD ) ) {
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php", MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/add_istanza_prodotto.php' method='post' id='istanza_prodotto_form'>
                <div>
                    <fieldset class='module aligned'>
                    <div class='form-row nome'>
                    <div>
                        <label for='id_nome'>Nome Prodotto Generico:</label><select name='prodotto_generico_id' id='id_nome'>
                        <option value='' selected='selected'>---------</option>
                        ".getProdottiGenericiOptions()."
                        </select>
                        <a href='./admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_prodotto_generico' onclick='return showAddAnotherPopup(this);'> <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/></a>

                        <p class='help'>Dopo aver inserito un nuovo prodotto generico, per favore aggiorna la pagina.</p>
                    </div>
                    </div>
                    <div class='form-row attivo'>
                        <div>
                            <label for='id_attivo' class='required'>Attivo</label><input type='checkbox' name='attivo' id='id_attivo' /><label for='id_attivo' class='vCheckboxLabel'>Attivo</label>
                        </div>
                    </div>
                    <div class='form-row colore'>
                        <div>
                            <label for='id_colore'>Colore:</label><select name='colore_id' id='id_colore'>
                            <option value='' selected='selected'>---------</option>
                            ".getColoriOptions()."
                            </select>
                            <a href='./admin_colori.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_colore' onclick='return showAddAnotherPopup(this);'> <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/></a>

                            <p class='help'>Dopo aver inserito un nuovo colore, per favore aggiorna la pagina.</p>
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>

           <script type='text/javascript'>document.getElementById('id_nome').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_CHANGE ) and ( isset($_GET['id'] ) ) ) {
        
        $id = $_GET['id'];
        $prodotto = getIstanzaProdottoById( $id );
		$attivo = $prodotto['attivo'];
		$prodotto_generico_id = $prodotto['prodotto_generico_id'];
		$colore_id = $prodotto['colore_id'];

		$checked = "";
		if ( $attivo == true )
		    $checked = "checked";
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php", MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/mod_istanza_prodotto.php' method='post' id='istanza_prodotto_form'>
                <div>
                    <fieldset class='module aligned'>
                    <div class='form-row nome'>
                        <div>
                            <label for='id_nome'>Nome Prodotto Generico:</label><select name='prodotto_generico_id' id='id_nome'>"
                            .getProdottoGenericoSelectedById( $prodotto_generico_id )."</select>
                            <a href='./admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_prodotto_generico' onclick='return showAddAnotherPopup(this);'> 
                                <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/>
                            </a>
                            <p class='help'>Dopo aver inserito un nuovo prodotto generico, per favore aggiorna la pagina.</p>
                        </div>
                    </div>
                    <div class='form-row attivo'>
                        <div>
                            <label for='id_attivo' class='required'>Attivo</label><input type='checkbox' name='attivo' id='id_attivo' checked='".$checked."' /><label for='id_attivo' class='vCheckboxLabel'>Attivo</label>
                        </div>
                    </div>
                    <div class='form-row colore'>
                        <div>
                            <label for='id_colore'>Colore:</label><select name='colore_id' id='id_colore'>"
                            .getColoreSelectedById( $colore_id )."</select>
                            <a href='./admin_colori.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_colore' onclick='return showAddAnotherPopup(this);'> 
                                <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/>
                            </a>
                            <p class='help'>Dopo aver inserito un nuovo colore, per favore aggiorna la pagina.</p>
                        </div>
                    </div>
                    <div class='form-row prodotti_correlati'>
                        <div>
                            <label for='id_colore'>Prodotti Correlati:</label>"
                            .buildCheckBoxProdottiCorrelatiById( $id )."
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>";

          echo "<input type='hidden' name='istanza_prodotto_id' value='".$id."' />";
          
          echo "<script type='text/javascript'>document.getElementById('id_title').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else {
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once( '../templates/crud/head.php' );
    
    echo "<body class='change-list'>";

    include('../templates/header.php');
    
    echo "<!-- Container -->
    <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php", MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE );

    if ( $_SESSION['added'] === 1) {
        echo "<ul class='messagelist'><li>Istanza prodotto aggiunta con successo.</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['added'] === 0) {
	       echo "<ul class='errorlist'><li>Impossibile aggiungere l'istanza di prodotto.</li></ul>";
	       unset($_SESSION['added']);
	} else if ( $_SESSION['deleted'] === 1) {
        echo "<ul class='messagelist'><li>Istanza/e di prodotto/i cancellata/e con successo</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['deleted'] === 0) {
        echo "<ul class='errorlist'><li>C'&egrave; qualche problema in fase di cancellazione. Eliminate ".$_SESSION['cancellati']." istanze di prodotti su ".$_SESSION['cancellati']+$_SESSION['scartati']." selezionate.</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['changed'] === 1) {
        echo "<ul class='messagelist'><li>Istanza di prodotto modificata con successo.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['changed'] === 0) {
	    echo "<ul class='messagelist'><li>Impossibile modificare L'istanza di prodotto.</li></ul>";
	    unset($_SESSION['changed']);
    }
    
    echo "<!-- Content -->
        <div id='content' class='flex'>
            <h1>Scegli Istanza di Prodotto da modificare</h1>
            <div id='content-main'>
                <ul class='object-tools'>"
                . getAddLink( ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_ADD, MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI_SINGOLARE ).
                "</ul>

          <div class='module' id='changelist'>          
              <form action='../form_actions/del_istanze_prodotti.php' method='post'>
                  <div class='actions'>
                      <label>Azione: <select name='action'>
                          <option value='' selected='selected'>---------</option>
                          <option value='delete_selected'>Cancella Istanza/e di Prodotto/i selezionata/e</option>
                      </select>
                      </label>
                      <button type='submit' class='button' title='Esegui l'azione selezionata' name='index' value='0'>Vai</button>
                  </div>
                <table cellspacing='0'>
                    <thead>
                        <tr>
                            <th>
                                <input type='checkbox' id='action-toggle' />
                            </th>
                            <th>
                                Nome Prodotto Generico
                            </th>
                            <th>
                                Categoria
                            </th>
                            <th>
                                Colore
                            </th>
                            <th>
                                Attivo
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    "
                    .getIstanzaDiProdottoConColore().
                    "
                    </tbody>
                </table>"
                . getPaginator( PRODOTTO_TABLE, MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI ) .
            "</form>
          </div> <!-- end class module -->
        </div> <!-- end class content-main -->
        <br class='clear' />
    </div>
    <!-- END Content -->";
    
    include_once('../templates/footer.php');
    
   echo "</div>
   <!-- END Container -->
    </body>
    </html>";
    }
} else {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}
?>