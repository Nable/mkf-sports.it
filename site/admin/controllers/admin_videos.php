<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');  
session_start();
if ( isset ( $_SESSION ['logged'] ) ) {
    if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_ADD ) ) {
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php", MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE );

        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/add_videos.php' method='post' id='avatar_form' enctype='multipart/form-data'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row titolo  '>
                        <div>
                            <label for='id_nome' class='required'>Nome:</label>
                            <input id='id_nome' type='text' class='vTextField' name='nome' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row file'>
            			<div>
                    		<label for='id_file'>File:</label>
                            <input type='file' name='image' id='id_file' />
		                </div>
        			</div>
                    <div class='form-row prodotto  '>
                        <div>
                            <label for='id_prodotto'>Prodotto:</label><select name='prodotto' id='id_prodotto'>
                            <option value='' selected='selected'>---------</option>
                            ".getProdottoOptions()."
                            </select><a href='./admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_ADD."#' class='add-another' id='add_id_prodotto' onclick='return showAddAnotherPopup(this);'> <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/></a>
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>

           <script type='text/javascript'>document.getElementById('id_titolo').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_CHANGE ) and ( isset($_GET['id'] ) ) ) {
        
        $id = $_GET['id'];
        
        $avatar = getVideoById($id); 
        
        $titolo = $avatar['nome'];
        $avatar_file = $avatar['file'];
        
        $file_name = str_replace ( UPLOAD_IMAGES_PATH."/video/" , "" , $avatar_file);
        
        $prodotto_id = $avatar['prodotto_id'];
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php", MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE );

        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Modifica ".MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/mod_videos.php' method='post' id='categoria_form' enctype='multipart/form-data'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row titolo  '>
                        <div>
                            <label for='id_nome' class='required'>Titolo:</label>
                            <input id='id_nome' type='text' class='vTextField' name='nome' value='".$titolo."' maxlength='255' />
                        </div>
                    </div>
                    <div class='form-row file'>
                       	<div>
                    		<label for='id_file'>File:</label>
                             Attualmente: <a target='_blank' href='".$avatar_file."'>".$avatar_file."</a>
                             <br>Modifica: <input name='image' id='id_file' type='file'>
                        </div>
                    </div>    
                    <div class='form-row clientela  '>
                        <div>
                            <label for='id_prodotto'>Prodotto:</label><select name='prodotto' id='id_prodotto'>"
                            .getProdottoSelectedById( $prodotto_id ).
                            "</select><a href='./admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_ADD."' class='add-another' id='add_id_clientela' onclick='return showAddAnotherPopup(this);'> <img src='".ADMIN_IMAGES_PATH."/icon_addlink.gif' width='10' height='10' alt='Aggiungi un Altro'/></a>
                        </div>
                    </div>
                    <input type='hidden' name='old_file' id='old_file' value=".$file_name." />
                    <input type='hidden' name='id_avatar' id='id_avatar' value=".$id." />
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>";

          echo "<input type='hidden' name='categoria_id' value='".$id."' />";
          
          echo "<script type='text/javascript'>document.getElementById('id_title').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else {
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once( '../templates/crud/head.php' );
    
    echo "<body class='change-list'>";

    include('../templates/header.php');
    
    echo "<!-- Container -->
    <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php", MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE );
        
    if ( $_SESSION['added'] === 1) {
        echo "<ul class='messagelist'><li>Video aggiunto con successo.</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['added'] === 0) {
	       echo "<ul class='errorlist'><li>Impossibile aggiungere il video.</li></ul>";
	       unset($_SESSION['added']);
	} else if ( $_SESSION['deleted'] === 1) {
        echo "<ul class='messagelist'><li>Video cancellato con successo</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['deleted'] === 0) {
        echo "<ul class='errorlist'><li>C'&egrave; qualche problema in fase di cancellazione. Eliminate ".$_SESSION['cancellati']." video su ".$_SESSION['cancellati']+$_SESSION['scartati']." selezionati.</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['changed'] === 1) {
        echo "<ul class='messagelist'><li>Video modificata con successo.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['changed'] === 0) {
	    echo "<ul class='messagelist'><li>Impossibile modificare il video.</li></ul>";
	    unset($_SESSION['changed']);
    }
    
    echo "<!-- Content -->
        <div id='content' class='flex'>
            <h1>Scegli Video da modificare</h1>
            <div id='content-main'>
                <ul class='object-tools'>"
                . getAddLink( ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php?op=".ADMIN_OP_TYPE_ADD, MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE ).
                "</ul>

          <div class='module' id='changelist'>          
              <form action='../form_actions/del_videos.php' method='post'>
                  <div class='actions'>
                      <label>Azione: <select name='action'>
                          <option value='' selected='selected'>---------</option>
                          <option value='delete_selected'>Cancella Video selezionato/i</option>
                      </select>
                      </label>
                      <button type='submit' class='button' title='Esegui l'azione selezionata' name='index' value='0'>Vai</button>
                  </div>
                <table cellspacing='0'>
                    <thead>
                        <tr>
                            <th>
                                <input type='checkbox' id='action-toggle' />
                            </th>
                            <th>
                                Nome
                            </th>
                            <th>
                                Prodotto
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    "
                    .getVideos().
                    "
                    </tbody>
                </table>"
                . getPaginator( VIDEO_TABLE, MODULE_PROPRIETA_MODEL_VIDEO_SINGOLARE ) .
            "</form>
          </div> <!-- end class module -->
        </div> <!-- end class content-main -->
        <br class='clear' />
    </div>
    <!-- END Content -->";
    
    include_once('../templates/footer.php');
    
   echo "</div>
   <!-- END Container -->
    </body>
    </html>";
    }
} else {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}
?>