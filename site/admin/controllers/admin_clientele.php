<?php
include_once('../../config/admin_messages.inc');
include_once('../../config/database.inc');
include_once('../../config/settings.inc');
include_once('../../libraries/util.lib.php');
session_start();

if ( isset ( $_SESSION ['logged'] ) ) {
    if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_ADD ) ) {

        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php", MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE );
       
        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/add_clientela.php' method='post' id='clientela_form'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row titolo  '>
                        <div>
                            <label for='id_titolo' class='required'>Titolo:</label><input id='id_titolo' type='text' class='vTextField' name='titolo' maxlength='255' />
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>

           <script type='text/javascript'>document.getElementById('id_titolo').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else if ( isset( $_GET['op'] ) and ( $_GET['op'] == ADMIN_OP_TYPE_CHANGE ) and ( isset($_GET['id'] ) ) ) {
        
        $id = $_GET['id'];
        $titolo = getTitoloClientelaById( $id );
        
        echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

        include_once( '../templates/crud/head.php' );

        echo "<body class='news-news change-form'>";

        include('../templates/header.php');

        echo "<!-- Container -->
        <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php", MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE );



        echo "<!-- Content -->
        <div id='content' class='colM'>
            <h1>Aggiungi ".MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE."</h1>
            <div id='content-main'>
                <form  action='../form_actions/mod_clientela.php' method='post' id='clientela_form'>
                <div>
                    <fieldset class='module aligned '>
                    <div class='form-row titolo  '>
                        <div>
                            <label for='id_titolo' class='required'>Titolo:</label><input id='id_titolo' type='text' class='vTextField' name='titolo' value='".$titolo."' maxlength='255' />
                        </div>
                    </div>
                    </fieldset>

                <div class='submit-row' >
                    <input type='submit' value='Salva' class='default' name='_save' />
                    <input type='submit' value='Salva e aggiungi un altro' name='_addanother'  />
                    <input type='submit' value='Salva e continua le modifiche' name='_continue' />
                </div>";
                
          echo "<input type='hidden' name='clientela_id' value='".$id."' />";

          echo "<script type='text/javascript'>document.getElementById('id_titolo').focus();</script>
           <script type='text/javascript'></script>
        </div>
        </form>
        </div>
        <br class='clear' />
        </div>

        <!-- END Content -->

        <div id='footer'></div>
        </div>
        <!-- END Container -->

        </body>
        </html>";

    } else {
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once( '../templates/crud/head.php' );
    
    echo "<body class='change-list'>";

    include('../templates/header.php');
    
    echo "<!-- Container -->
    <div id='container'>"

        .getBreadcrumbs( MODULE_PROPRIETA, ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php", MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE );

    if ( $_SESSION['added'] === 1) {
        echo "<ul class='messagelist'><li>Clientela aggiunta con successo.</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['added'] === 0) {
        echo "<ul class='errorlist'><li>Impossibile aggiungere la clientela.</li></ul>";
        unset($_SESSION['added']);
    } else if ( $_SESSION['changed'] === 1 ) {
        echo "<ul class='messagelist'><li>Clientela modificata con successo.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['changed'] === 0 ) {
        echo "<ul class='errorlist'><li>Impossibile modificare la clientela.</li></ul>";
        unset($_SESSION['changed']);
    } else if ( $_SESSION['deleted'] === 1) {
        echo "<ul class='messagelist'><li>Clientela/e cancellata/e con successo</li></ul>";
        unset($_SESSION['deleted']);
    } else if ( $_SESSION['deleted'] === 0) {
        echo "<ul class='errorlist'><li>C'&egrave; qualche problema in fase di cancellazione. Eliminate ".$_SESSION['cancellati']." clientele su ".$_SESSION['cancellati']+$_SESSION['scartati']." selezionate.</li></ul>";
        unset($_SESSION['deleted']);
    }
    
    echo "<!-- Content -->
        <div id='content' class='flex'>
            <h1>Scegli Clientela da modificare</h1>
            <div id='content-main'>
                <ul class='object-tools'>"
                . getAddLink( ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php?op=".ADMIN_OP_TYPE_ADD, MODULE_PROPRIETA_MODEL_CLIENTELE_SINGOLARE ).
                "</ul>

          <div class='module' id='changelist'>
              <form action='../form_actions/del_clientele.php' method='post'>
                  <div class='actions'>
                      <label>Azione: <select name='action'>
                          <option value='' selected='selected'>---------</option>
                          <option value='delete_selected'>Cancella Clientela/e selezionati/e</option>
                      </select>
                      </label>
                      <button type='submit' class='button' title='Esegui l'azione selezionata' name='index' value='0'>Vai</button>
                  </div>
                <table cellspacing='0'>
                    <thead>
                        <tr>
                            <th>
                                <input type='checkbox' id='action-toggle' />
                            </th>
                            <th>
                                Titolo Clientela
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    "
                    .getClientela().
                    "
                    </tbody>
                </table>"
                . getPaginator( CLIENTELA_TABLE, MODULE_PROPRIETA_MODEL_CLIENTELE ) .
            "</form>
          </div> <!-- end class module -->
        </div> <!-- end class content-main -->
        <br class='clear' />
    </div>
    <!-- END Content -->";
    
    include_once('../templates/footer.php');
    
   echo "</div>
   <!-- END Container -->
    </body>
    </html>";
    }
} else {
    reindirizza( ADMIN_ABSOLUTE_URL.'/index.php');
    exit ();
}
?>