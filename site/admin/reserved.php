<?php
include_once('../config/admin_messages.inc');
include_once('../config/database.inc');
include_once('../config/settings.inc');
include_once('../libraries/util.lib.php');  
session_start();
if ( isset ( $_SESSION ['logged'] )) { 
    echo "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' lang='it-it' xml:lang='it-it' >";

    include_once('./templates/head.php');
    echo "<body class='dashboard'>

    <!-- Container -->

    <div id='container'>";
    
        include_once('./templates/header.php');

        echo "<!-- Content -->
        <div id='content' class='colMS'>";

            // le variabili son state caricate nell'header.php
            echo "<h1>".ADMIN_MAIN_TITLE."</h1>";

            include_once('./templates/log.php');
        
            // Modulo sezione ADMIN
            echo "<div id='content-main'>
            <div class='module'>
                <table summary='Modelli disponibili nell'applicazione Admin.'>
                    <caption>".MODULE_ADMIN."</caption>
                    <tr>
                        <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php'>".MODULE_ADMIN_MODEL_USERS."</a></th>
                        <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                        <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_utenti.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                    </tr>
                </table>
            </div>   
        </div>";
    
        // Modulo sezione MEDIA
        echo "<div id='content-main'>
        <div class='module'>
            <table summary='Modelli disponibili nell'applicazione Media.'>
                <caption>".MODULE_MEDIA."</caption>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_avatars.php'>".MODULE_MEDIA_MODEL_AVATARS."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_avatars.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_avatars.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_dettaglio.php'>".MODULE_MEDIA_MODEL_IMMAGINI_DETTAGLIO."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_dettaglio.php?op=".ADMIN_OP_TYPE_ADD."'class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_dettaglio.php?op=".ADMIN_OP_TYPE_CHANGE."'class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_rotazione.php'>".MODULE_MEDIA_MODEL_IMMAGINI_ROTAZIONE."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_rotazione.php?op=".ADMIN_OP_TYPE_ADD."'class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_immagini_rotazione.php?op=".ADMIN_OP_TYPE_CHANGE."'class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php'>".MODULE_MEDIA_MODEL_VIDEO."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php?op=".ADMIN_OP_TYPE_ADD."'class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_videos.php?op=".ADMIN_OP_TYPE_CHANGE."'class='changelink'>Modifica</a></td>
                </tr>
            </table>
        </div>
    </div>";

        // Modulo sezione PRODOTTI
        echo "<div id='content-main'>
        <div class='module'>
            <table summary='Modelli disponibili nell'applicazione Prodotti.'>
                <caption>".MODULE_PRODOTTI."</caption>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php'>".MODULE_PRODOTTI_MODEL_PRODOTTI_GENERICI."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_generici.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php'>".MODULE_PRODOTTI_MODEL_ISTANZE_PRODOTTI."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_ADD."'class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_istanze_prodotti.php?op=".ADMIN_OP_TYPE_CHANGE."'class='changelink'>Modifica</a></td>
                </tr>
                <!--<tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_correlati.php'>".MODULE_PRODOTTI_MODEL_PRODOTTI_CORRELATI."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_correlati.php?op=".ADMIN_OP_TYPE_ADD."'class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_prodotti_correlati.php?op=".ADMIN_OP_TYPE_CHANGE."'class='changelink'>Modifica</a></td>
                </tr>-->
            </table>
        </div>
    </div>";

        // Modulo sezione PROPRIETA'
        echo "<div id='content-main'>
        <div class='module'>
            <table summary='Modelli disponibili nell'applicazione PROPRIETA.'>
                <caption>".MODULE_PROPRIETA."</caption>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_categorie.php'>".MODULE_PROPRIETA_MODEL_CATEGORIE."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_categorie.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_categorie.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php'>".MODULE_PROPRIETA_MODEL_CLIENTELE."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_clientele.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                </tr>
                <tr>
                    <th scope='row'><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_colori.php'>".MODULE_PROPRIETA_MODEL_COLORI."</a></th>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_colori.php?op=".ADMIN_OP_TYPE_ADD."' class='addlink'>Aggiungi</a></td>
                    <td><a href='".ADMIN_CONTROLLERS_ABSOLUTE_URL."/admin_colori.php?op=".ADMIN_OP_TYPE_CHANGE."' class='changelink'>Modifica</a></td>
                </tr>
            </table>
        </div>
    </div>";

    echo"<br class='clear' />
    </div>
    <!-- END Content -->";

    include_once('./templates/footer.php');
    echo"</div>
    <!-- END Container -->

    </body>
    </html>";
} else {
    reindirizza( ABSOLUTE_URL."/admin/index.php");
    exit ();
}
?>