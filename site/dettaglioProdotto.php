<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" > 
<html>
<head>
<?php
	include_once('config/database.inc');	
    include_once('libraries/database.lib.php');
    include_once('libraries/util.lib.php');
    
	$id_prodotto = $_GET['id'];
	$id_posizione = $_GET['pos'];
	
	$array_generico = getProdottoGenericoByIdVetrina($id_prodotto);
	$id_generico = $array_generico['id'];
	$nomeProdotto = $array_generico['nome'];
	$descrizioneProdotto = $array_generico['descrizione'];

	$array_rotazione = getImmagineRotazioneByIdVetrina($id_prodotto);
	if($id_posizione==1)
		$immagineProdotto = $array_rotazione['file1'];
	else if ($id_posizione==2)
		$immagineProdotto = $array_rotazione['file2'];
	else if($id_posizione==3)
		$immagineProdotto = $array_rotazione['file3'];
	$estensione = getEstensione($immagineProdotto);
	
	//$immagineProdotto = "http://localhost/mkf-sport/resources/images/upload/rotazione/maglia.jpg";
    $testoImmagine = $nomeProdotto;
    $titoloImmagine = $nomeProdotto;
?>



    <title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<! -- link rel="stylesheet" href="css/prettyPhoto.html" media="screen" / -->
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
	<script type="text/javascript">
		var JQ = jQuery.noConflict();
	</script>
	<script src="js_p/raw_p.js"></script>
	<script src="js_p/jquery.prettyPhoto_p.js"></script>  
	<script src="js_p/jquery.backstretch_p.min.js"></script>
	
	<script type="text/javascript">
	
	
	var total=0;
	
	    JQ(window).load(function() {
	          
		  JQ('#main').width(JQ('#immagineProdotto').width());
		  JQ('#main').height(JQ('#immagineProdotto').height());
	    });

	    
		JQ(document).ready(function(){
		    
		    //JQ("a[rel^='prettyPhoto']").prettyPhoto({theme: 'dark_square'});
		    //JQ.backstretch("images/background.jpg", {speed: 'slow'});
		    
		    JQ('#main').width(JQ('#immagineProdotto').width());
		    JQ('#main').height(JQ('#immagineProdotto').height());
		    
		    JQ(window).resize( function() {
		   
		       console.log("Window " + window.innerWidth + ", " + window.innerHeight);
		       //JQ('#main').height(window.innerHeight);
		       //console.log("Main " + JQ('#main').width() + ", " + JQ('#main').height());
		       
		       JQ('#botteneAggiorna').css('position', 'absolute');
		       JQ('#bottoneAggiorna').css('left', window.innerWidth / 2);
		       JQ('#bottoneAggiorna').css('top', window.innerHeight / 2);
		       JQ('#aggiorna').css('width', window.innerWidth );
		       JQ('#aggiorna').css('height', window.innerHeight );
		       JQ('#aggiorna').show();
		       
		    });
		    
		    new Zoomer('image');
		    
		});
		
		
		
		
		function aggiornaPagina() {
		    
		    JQ('#aggiorna').hide();
		    location.reload(true);
		    
		}
		
	</script>
	
	
	    <script type="text/javascript" src="vetrina/js/prototype.js"></script>
    <script type="text/javascript" src="vetrina/js/zoomer.js"></script>

    
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        
        body {
            background-color: #c2d7ff;
            background-color: #808ea8;
            background-color: #FFF;
            font-size: 12px;
            font-family: Verdana, Arial, Sans;
        }

        h1 {
            font-size: 16px;
            font-weight: bold;
            padding-bottom: 3px;
        }

        h2 {
            font-size: 12px;
            font-style: italic;
        }

        a {
            
            color: #FFF;
        }

        ul {
            list-style-type: none;
            margin-top: 10px;
        }

        ul li {
            display: inline;
        }

        #main {
            
            /*padding: 10px;*/
            margin: 0px auto 0 auto;
            background-color: #fff;
            /*border: 1px solid #808ea8;
	    box-shadow: 0 1px 4px #000000;*/
	
        }	
	

        p {
            padding: 10px 0;
        }

        #container {
            /*border: 1px solid #808ea8;*/
        }

        img {
            border: 0;
        }

        a img {
            display: block;
        }
	
	#chiudi {
	    position:fixed;
	    top:5px;
	    right:0;
	    width:200px;
	    background-color: #fff;
	    padding-left: 5px;
	    z-index: 101;
	}
	
	#chiudi a {
	    text-decoration: none;
	}
	
	#chiudi a:hover {
	    font-weight: bolder;
	}
	
	#immagineProdotto  {
	        cursor: crosshair;
	}
	
	.css3button {
		
		font-family: Arial, Helvetica, sans-serif;
		
		font-size: 12px;
		
		color: #fff;
		
		padding: 5px;
		
		background: -moz-linear-gradient(
			top,
			#42aaff 0%,
			#003366);
		
		background: -webkit-gradient(
			linear, left top, left bottom, 
			from(#42aaff),
			to(#003366));
		
		border-radius: 10px 0px 0px 10px;
		-moz-border-radius: 10px 0px 0px 10px;
		-webkit-border-radius: 10px 0px 0px 10px;
		
		border: 3px solid #003366;
		-moz-box-shadow:
			3px 3px 3px rgba(000,000,000,0.3),
			inset 0px 0px 1px rgba(255,255,255,0.5);
		-webkit-box-shadow:
			3px 3px 3px rgba(000,000,000,0.3),
			inset 0px 0px 1px rgba(255,255,255,0.5);
		
		text-shadow:
			0px -1px 0px rgba(000,000,000,0.7),
			0px 1px 0px rgba(255,255,255,0.3);
	}
	
	.css3button :hover {
	    color: #fff;
	}
		
	.posizioneTitolo {
	    position:fixed;
	    top:20px;
	    left:0;
	    -moz-box-shadow:inset 0px 1px 10px 0px #ffffff;
	    -webkit-box-shadow:inset 0px 1px 10px 0px #ffffff;
	    box-shadow:inset 0px 1px 10px 0px #ffffff;
	    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	    background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	    background-color:#ededed;
	    border:2px solid #dcdcdc;
	    display:inline-block;
	    color:#777777;
	    font-family:arial;
	    font-size:24px;
	    font-weight:bold;
	    padding:7px 76px;
	    text-decoration:none;
	    text-shadow:3px 1px 0px #ffffff;
	}
	
	.posizioneTitolo:hover {
	    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	    background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	    background-color:#dfdfdf;
	}
	
	.posizioneTitolo:active {
	    position:relative;
	    top:1px;
	}
	
	
	#aggiorna {
	    position: absolute;
	    top: 0px;
	    left: 0px;
	    width: 100%;
	    height: 100%;
	    background-color: #abc;
	    z-index: 99;
	    display: none;
	    opacity:.80;
	    filter:alpha(opacity=80);
	    filter:�alpha(opacity=80)�;
	}
	
	#bottoneAggiorna {
	    
		position: absolute;
		top: window.innerWidth/2;
		left: window.innerHeight/2;
	    
		width: 100px;
		
		opacity:1;
	    filter:alpha(opacity=100);
	    filter:�alpha(opacity=100)�;
		
		font-family: Arial, Helvetica, sans-serif;
		
		font-size: 12px;
		
		text-align: center;
		
		color: #fff;
		
		padding: 5px;
		
		background: -moz-linear-gradient(
			top,
			#42aaff 0%,
			#003366);
		
		background: -webkit-gradient(
			linear, left top, left bottom, 
			from(#42aaff),
			to(#003366));
		
		border-radius: 0px;
		-moz-border-radius: 0px;
		-webkit-border-radius: 0px;
		
		border: 3px solid #003366;
		-moz-box-shadow:
			3px 3px 3px rgba(000,000,000,0.3),
			inset 0px 0px 1px rgba(255,255,255,0.5);
		-webkit-box-shadow:
			3px 3px 3px rgba(000,000,000,0.3),
			inset 0px 0px 1px rgba(255,255,255,0.5);
		
		text-shadow:
			0px -1px 0px rgba(000,000,000,0.7),
			0px 1px 0px rgba(255,255,255,0.3);
	}
    </style>

</head>
<body>
    <div id="aggiorna">
	
	<div id="bottoneAggiorna">
		<a href="javascript:aggiornaPagina();">Aggiorna
		</a>
	</div>
	
    </div>
    <div id="chiudi" class="css3button">
	<a href="vetrina.php?id=<?php echo $id_prodotto?>">
	    X CHIUDI
	</a>
    </div>
    <div id="info" class="posizioneTitolo">
	<?php echo $nomeProdotto?>
    </div>
    <div id="main">
        <div id="container">
            <a href="<?php echo $immagineProdotto ?>" id="image">
		<!-- LA RIGA SEGUENTE -->
		<img id="immagineProdotto" src="<?php echo $immagineProdotto.".resized".$estensione ?>" />
		<!-- DEVE DIVENTARE COSI-->
		
		<!-- FINE MODIFICA -->
	    </a>
        </div>
    </div>
    <script type="text/javascript">
    </script>
    
    
</body>
</html>
