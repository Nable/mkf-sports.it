<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/typography.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:59 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_v.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="clearfix">
		
		<div class="article-wrapper">
			
			<!-- CONTENT -->
			<article>
			
				<h1>Typography</h1>
				
				<h3>Paragraphs</h3> 
				
				<p>Murpis ipsum, blandit eu semper in, tincidunt at libero. <a href="http://google.com/">Cras nec felis libero</a>. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor <a href="http://raw-brand.com/wp3/spektrum/">malesuada tincidunt lectus</a>. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p> 
				<p>Murpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p> 
				
				<h1>Heading 1</h1> 
				<h2>Heading 2</h2> 
				<h3>Heading 3</h3> 
				<h4>Heading 4</h4> 
				<h5>Heading 5</h5> 
				<h6>Heading 6</h6> 
				
				<div class="half"> 

					<h3>Dropcap 1</h3> 
				
					<p><span class="dropcap_1">N</span>am turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p>
				 
				</div> 

				<div class="half end"> 
				
					<h3>Dropcap 2</h3>
					
					<p><span class="dropcap_2">1</span>Ham turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p>
	 
				</div>
				
				<div class="clearboth"></div>

				<h3>Tables</h3>
				
				<table> 
					<thead> 
						<tr> 
							<th>Configurations</th> 
							<th>Dual 1.8GHz</th> 
							<th>Dual 2GHz</th> 
							<th>Dual 2GHz</th> 
						</tr> 
					</thead> 
					<tbody> 
						<tr> 
							<td></td> 
							<td>M9454LL/A</td> 
							<td>M9455LL/A</td> 
							<td>M9457LL/A</td> 
						</tr> 
						<tr class="alt"> 
							<td></td> 
							<td>M9454LL/A</td> 
							<td>M9455LL/A</td> 
							<td>M9457LL/A</td> 
						</tr> 
						<tr> 
							<td></td> 
							<td>M9454LL/A</td> 
							<td>M9455LL/A</td> 
							<td>M9457LL/A</td> 
						</tr> 
					</tbody> 
				</table>
				
				<div class="third"> 

					<h3>Unordered Lists</h3>
					
					<ul> 
						<li>List item</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item
							<ol> 
								<li>List item</li> 
								<li>List item</li> 
							</ol> 
						</li> 
						<li>List item</li> 
					</ul> 
				 
				</div>
				
				<div class="third"> 

					<h3>Check List</h3> 

					<ul class="check"> 
						<li>List item</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
					</ul>
					
				</div>
				
				<div class="third end"> 
				
					<h3>Cross List</h3> 
		
					<ul class="cross"> 
						<li>List item</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
					</ul>
					
				</div>
				
				<div class="third">
					
					<h3>Circle List</h3> 
					
					<ul class="circle"> 
						<li>List item</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
					</ul>
					
				</div>
				
				<div class="third"> 

					<h3>Square List</h3> 
					
					<ul class="square"> 
						<li>List item</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
						<li>List item</li> 
					</ul> 
				</div>
				
				<div class="third end"> 

					<h3>Ordered Lists</h3> 
					
					<ol> 
						<li>List item</li> 
						<li>List item
							<ol> 
								<li>List item</li> 
								<li>List item</li> 
							</ol> 
						</li> 
						<li>List item
							<ul> 
								<li>List item</li> 
								<li>List item</li> 
							</ul> 
						</li> 
						<li>List item</li> 
						<li>List item</li> 
					</ol> 
 
				</div>
				
				<div class="clearboth"></div> 
			
				<div class="third"> 

					<h3>Third</h3>
					<p>Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div>
				
				<div class="third"> 
			
					<h3>Third</h3> 
					<p>Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div>
				
				<div class="third end"> 

					<h3>Third</h3> 
					<p>Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div> 
				
				<div class="half"> 
					<h3>Half</h3> 
					<p>Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p> 
				 
				</div> 
				
				<div class="half end"> 
				
					<h3>Half</h3> 
					<p>Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p> 
				 
				</div> 
				
				<div class="two-thirds"> 
				
					<h3>twothirds</h3> 
					<p>Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div> 

				<div class="third end"> 

					<h3>third</h3> 
					<p>Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div> 
				
				<div class="third"> 
				
					<h3>third</h3> 
					<p>Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div> 
				
				<div class="two-thirds end"> 
			
					<h3>twothirds</h3> 
					<p>Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium.</p> 
				 
				</div>		
				
				<div class="clearboth"></div> 
				
				<h2>Quotes</h2>
				
				<blockquote> 
				
					<p>Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus.  Maecenas lacinia justo eu massa elementum pretium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p>
				
				</blockquote>
				
				<div class="clearboth"></div>
			
				<p><span class="quote_left">"A left pullquote. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus."</span>Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p>
				
				<div class="clearboth"></div>
				
				<p><span class="quote_right">"A right pullquote. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus."</span> Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Suspendisse quis enim id erat imperdiet tempor malesuada tincidunt lectus. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero.</p>
				
				<div class="clearboth"></div>
				
				<h2>Highlight</h2>
				<p>Some dummy text to <span class="highlight">show highlighting</span>.</p>

				<h2>Divider</h2>
				<hr>
				
				<h2>Thin Divider</h2>
				<hr class="thin">
				
				<h2>Divider With Top Link</h2>
				<div class="divider top"><a href="#top">top</a></div>
				
				<div class="third">
			
					<h2>download</h2>
					<div class="download_box">
						<p><a href="#">Download</a> 25mb</p>
					</div>

				</div>

				<div class="third">
		
					<h2>Info</h2>
					<div class="info_box">
						
						<p>This is some info.</p>

					</div>

				</div>
				
				<div class="third end">
								
					<h2>Warning</h2>
					<div class="warning_box">
								
						<p>This is a warning box.</p>

					</div>

				</div>
				
				<div class="clearboth"></div>
				
				<h3>Button</h3>
				<p><a href="http://www.raw-brand.com/" class="button left">A Button</a> <a href="http://www.raw-brand.com/" class="button left">Another Button That's Much Longer!</a></p>
				
				<div class="clearboth"></div>

				<h3>Toggle</h3>
				
				<div class="toggle-content expanding">
					<div class="expand-button">
						<p>This is the title of the toggle content</p>
					</div>
					<div class="expand">
						
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
						
					</div>
				</div>
				
				<div class="toggle-content expanding">
					<div class="expand-button">
						<p>This is the title of the toggle content</p>
					</div>
					<div class="expand">
						
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
						
					</div>
				</div> 
				
				<div class="toggle-content expanding">
					<div class="expand-button">
						<p>This is the title of the toggle content</p>
					</div>
					<div class="expand">
						
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
					<p>This is the content that will be toggled when the above title is clicked. Nam turpis ipsum, blandit eu semper in, tincidunt at libero. Cras nec felis libero. Integer interdum commodo laoreet. Suspendisse quis libero lacus. Maecenas lacinia justo eu massa elementum codetium.</p>
						
					</div>
				</div>
				
			</article>
		
		</div>
		
	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>

</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/typography.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:59 GMT -->
</html>