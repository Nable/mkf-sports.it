<?php
include_once('config/database.inc');
include_once('config/menu.php');
include_once('libraries/database.lib.php');

	

?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/page.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:23 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_v.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>
<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="#">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>
	
	<!-- CONTENT -->
	<div id="content" class="clearfix">
	
	<div class="article-wrapper">
			
			<!-- CONTENT -->
			<article>
<!---->			
		<div id="container">
  <div id="content" class="clearfix">
  
	
	<?php 
		if(isset( $_GET['s']) && $_GET['s'] != "") {?>
			<!-- INIZIO COLONNA 810 -->
  
     		<div class="col width_810 main">
        		<h4 class="nome-capo">Risultati Ricerca</h4>
        	</div> 
        	<!-- FINE COLONNA 810 -->
		
			<?php echo cercaProdotto($_GET['s']);
		} else {?>
			<!-- INIZIO COLONNA 810 -->
  
     		<div class="col width_810 main">
        		<h4 class="nome-capo">COLLEZIONE</h4>
        	</div> 
        	<!-- FINE COLONNA 810 -->
	<?php 
			
			$array_collezioni = getCollezioniElenco();
		
			for ($i = 0; $i < $array_collezioni['num']; $i++) {
	?>
		<!-- INIZIO COLONNA 810 -->
		
		
				<h4><?php echo $array_collezioni['nome'.$i] ?></h4>
		<div class='col width_810'>
			
			<div class='box'>			
					<?php 
						echo getProdottiElencoByCollezione($array_collezioni['id'.$i]);
					?>				
			</div> 					
		</div>
	<?php 		} 
			}	?>
		<!-- FINE COLONNA 810 -->	
    </div> 
</div> 	
<!---->			
			</article>
	</div>		
	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/home.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:23 GMT -->
</html>