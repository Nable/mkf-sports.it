<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/gallery.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:35 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_h.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="height-fix clearfix">

		<!-- BLOG ITEMS -->
		<div id="article-list">			
			
			<!-- ITEM 1 (TEXT) -->
			<div class="article-wrapper type-text print web-design">
			
				<article>
				
					<h1>Gallery Title</h1>
					
					<section>
						<p>Photography by <a href="http://www.flickr.com/photos/markjsebastian/">Mark Sebastian</a>.</p>
					</section>
						
				</article>				
				
			</div>
			
			<!-- Gallery 1 (IMAGE) -->
			<div class="article-wrapper type-image">
				
				<article>
					
					<img src="images/gallery1_thumb.jpg" alt="" />
				
					<footer class="post-meta">
						
						<a href="images/gallery1.jpg" rel="prettyphoto" class="more-link">View+</a>
						
					</footer>
					
				</article>
				
			</div>
			
			<!-- Gallery 2 (IMAGE) -->
			<div class="article-wrapper type-image">
				
				<article>
				
					<img src="images/gallery2_thumb.jpg" alt="" />
				
					<footer class="post-meta">
						
						<a href="images/gallery2.jpg" rel="prettyphoto" class="more-link">View+</a>
						
					</footer>
					
				</article>
				
			</div>
			
			<!-- Gallery 3 (IMAGE) -->
			<div class="article-wrapper type-image">
				
				<article>
					
					<img src="images/gallery3_thumb.jpg" alt="" />
				
					<footer class="post-meta">
						
						<a href="images/gallery3.jpg" rel="prettyphoto" class="more-link">View+</a>
						
					</footer>
					
				</article>
				
			</div>
			
		</div>
		
	</div>
	
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/gallery.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:59 GMT -->
</html>