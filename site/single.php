<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/single.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:25 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_v.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="clearfix">
		
		<div class="article-wrapper clearfix">
			
			<!-- CONTENT -->
			<article class="main">
			
				<h1>Single Post Template</h1>
				
				<div class="single-post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a>, <a href="#">CATEGORY 2</a></li>
						</ul>							
						
				</div>
				
				<!-- ARTICLE IMAGE -->
				<div id="feature-image">
				
					<img src="images/single-image.jpg" alt="" />
					
				</div>
				
				<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti. Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est. Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus. Nulla in egestas mauris. In a consectetur dui. Fusce mi felis, tincidunt a molestie sed, hendrerit in massa. Fusce justo lacus, cursus ac convallis auctor, imperdiet ut mi. Sed vitae urna erat. In neque eros, imperdiet eget euismod in, vulputate non neque.</p>
				
				<p>Proin mollis est ac magna pretium laoreet. Suspendisse potenti. Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est. Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus. Nulla in egestas mauris. In a consectetur dui. Fusce mi felis, tincidunt a molestie sed, hendrerit in massa.</p>
				
				<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti. Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est. Nulla in egestas mauris. In a consectetur dui. Fusce mi felis, tincidunt a molestie sed, hendrerit in massa. Fusce justo lacus, cursus ac convallis auctor, imperdiet ut mi. Sed vitae urna erat. In neque eros, imperdiet eget euismod in, vulputate non neque.</p>
				
				<!-- ABOUT AUTHOR -->
				<div id="author-bio">
				
					<img alt="" src="images/avatar.png" class="avatar avatar-64 photo" height="64" width="64" />			
				
					<div class="author-text">
						
						<h4>Author</h4>
						
						<div class="the-comment">
						
							<p>Proin mollis est ac magna pretium laoreet. Suspendisse potenti. Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est.</p>
						
						</div>
						
					</div>
				
				</div>
				
				<!-- COMMENTS -->
				<h2>3 Comments</h2>
				
				<ol id="comments" class="commentlist clearfix">
	
					<li class="comment even thread-even depth-1 clearfix" id="li-comment-11">
						
						<div class="comment-holder " id="comment-11">
	
							<img alt="" src="images/avatar.png" class="avatar avatar-64 photo" height="64" width="64" />			
		
							<div class="author-text">
		
								<small><time><a href="#">August 12, 2010 @ 12:18 pm</a></time></small>
			
								<h4><a href="#" rel="external nofollow" class="url">Name</a> says:</h4>
								
								<div class="the-comment">
								
									<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti.</p>
								
								</div>
								
								<a rel="nofollow" class="comment-reply-link" href="#">Reply</a>
								
							</div>
							
						</div>
						
						<ul class="children">

							<li class="comment odd alt depth-2 clearfix" id="li-comment-14">

								<div class="comment-holder " id="comment-14">
	
									<img alt="" src="images/avatar.png" class="avatar avatar-64 photo" height="64" width="64" />			
	
									<div class="author-text">
		
										<small><time><a href="#">August 12, 2010 @ 12:18 pm</a></time></small>
										
										<h4><a href="#" rel="external nofollow" class="url">Name</a> says:</h4>
										
										<div class="the-comment">
											
											<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti.</p>
										
										</div>
										
										<a rel="nofollow" class="comment-reply-link" href="#">Reply</a>
									
									</div>
									
									<ul class="children">
										
										<li class="comment even depth-3 clearfix" id="li-comment-15">
										
											<div class="comment-holder " id="comment-15">
											
												<img alt="" src="images/avatar.png" class="avatar avatar-64 photo" height="64" width="64" />			
												
												<div class="author-text">
												
													<small><time><a href="#">August 12, 2010 @ 12:18 pm</a></time></small>
													
													<h4><a href="#" rel="external nofollow" class="url">Name</a> says:</h4>
													
													<div class="the-comment">
													
														<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti.</p>
													
													</div>
													
													<a rel="nofollow" class="comment-reply-link" href="#">Reply</a>
													
												</div>
												
											</div>

										</li>
										
									</ul>
								
								</div>
								
							</li>
							
						</ul>
						
					</li>
					
				</ol>
				
				<!-- COMMENT FORM -->
				<div id="respond">
					<h2>Leave a comment</h2>
					<form action="#" id="commentform" method="post">
							
						<fieldset>
							
							<label for="contactName"><span>*</span> Name</label>																		
							<input type="text" name="contactName" id="contactName" value="" tabindex="1">
							
							<label for="email"><span>*</span> Email Address</label>
							<input type="text" name="email" id="email" value="">
							
							<label for="url">Website</label>
							<input type="text" name="url" id="url" value="">
							
							<label for="commentsText"><span>*</span> Message</label>
							<textarea name="comments" id="commentsText" rows="1" cols="1"></textarea>
							
							<input name="submit" type="submit" id="submit" class="comment button" tabindex="5" value="Send">
							
							<span class="displace"><label for="checking">If you want to submit this form, do not enter anything in this field</label><input type="text" name="checking" id="checking" value="" class="idle"></span>
							<input type="hidden" name="submitted" id="submitted" value="true">
							
						</fieldset>

					</form>
					
				</div>
				
			</article>
			
			<!-- SIDEBAR -->
			<ul id="article-sidebar">
				
				<!-- SELECT FIELDS -->
				<li id="categories-4" class="widget widget_categories">
				
					<h3 class="widgettitle">Categories</h3>
					
					<select name="cat" id="cat">
						<option value="-1">Select Category</option>
						<option>Child Category I</option>
						<option>Child Category II</option>
						<option>Child Category III</option>
						<option>Grandchild Category I</option>
						<option>Parent Category I</option>
						<option>Parent Category II</option>
						<option>Parent Category III</option>
					</select>
				
				</li>
						
				<!-- RECENT COMMENTS -->
				<li class="widget widget_recent_comments">
					
					<h3 class="widgettitle">Recent Commnets</h3>
					
					<ul id="recentcomments">
						<li class="recentcomments">
							<a href="#" rel="external nofollow" class="url">Joe Bloggs</a> on <a href="#">Blog Post One</a>
						</li>
						<li class="recentcomments">
							<a href="#" rel="external nofollow" class="url">Admin</a> on <a href="#">Blog Post One</a>
						</li>
						<li class="recentcomments">
							<a href="#" rel="external nofollow" class="url">Sarah</a> on <a href="#">Blog Post Two</a>
						</li>
						<li class="recentcomments">
							<a href="#" rel="external nofollow" class="url">Joe Bloggs</a> on <a href="#">Blog Post Three</a>
						</li>
					</ul>
				
				</li>
				
				<!-- TEXT WIDGET -->
				<li class="widget widget_text">
				
					<h3 class="widgettitle">Text</h3>
					
					<div class="textwidget">
						<p>Vestibulum viverra facilisis eros facilisis faucibus. Pellentesque sed lectus magna. Etiam nec purus ut odio malesuada hendrerit non at ipsum. Phasellus nisl enim, porta vitae commodo fringilla, faucibus consequat elit.</p>
					</div>
				
				</li>
				
				<!-- POPULAR POSTS -->
				<li class="widget raw_popular_posts">
				
					<h3 class="widgettitle">Popular Posts</h3>
						
					<ol id="raw_popular_posts">
					
						<li>
							<a href="#">
								<img width="60" height="60" src="../../raw-brand.com/wp3/neuebold/files/2010/12/Peaches-Music-55x55.jpg" alt="">
							</a>
							
							<h5><a href="#">Blog Post One</a></h5>
							<em>29 views</em>
						</li>
						
						<li>
							<a href="#">
								<img width="60" height="60" src="../../raw-brand.com/wp3/neuebold/files/2010/12/The-Show-55x55.jpg" alt="">
							</a>
							
							<h5><a href="#">Blog Post Two</a></h5>
							<em>41 views</em>
						</li>
						
						<li>
							<a href="#">
								<img width="60" height="60" src="../../raw-brand.com/wp3/neuebold/files/2010/12/The-DJ-55x55.jpg" alt="">
							</a>
							
							<h5><a href="#">Blog Post Three</a></h5>
							<em>36 views</em>
						</li>
					
					</ol>
					
				</li>
				
				<!-- SEARCH -->
				<li class="widget widget_search">
					
					<h3 class="widgettitle">Search</h3>
					
					<form method="get" class="searchform" action="http://raw-brand.com/wp3/neuebold/">
						<div>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s idle" type="text" value="" name="s">							
						</div>
					</form>
					
				</li>
				
				<!-- SOCIAL BUTTONS -->
				<li class="widget raw_social">
					
					<h3 class="widgettitle">Social Buttons</h3>
					
					<div class="social-button-holder">
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>						
					</div>
				
				</li>
				
				<!-- CATEGORY / ARCHIVE / PAGES -->
				<li class="widget widget_pages">
				
					<h3 class="widgettitle">Pages</h3>
					
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Services</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
					</ul>

				</li>				
			
			</ul>			
			
		</div>
	
	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/single.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:28 GMT -->
</html>