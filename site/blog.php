<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/blog.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:43:28 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_h.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="height-fix clearfix">

		<!-- BLOG ITEMS -->
		<div id="article-list">			
			
			<!-- BLOG POST 1 (TEXT) -->
			<div class="article-wrapper type-text">
			
				<article>
				
					<h1>Text Post</h1>
					
					<section>
						<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris.</p>
					</section>
					
					<footer class="post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a></li>
						</ul>
						
						<a href="single.html" class="more-link">More+</a>
						
					</footer>
					
				</article>				
				
			</div>
			
			<!-- BLOG POST 2 (IMAGE) -->
			<div class="article-wrapper type-image hover">
				
				<article>
				
					<h1><span>Post With Featured Image</span></h1>
					
					<img src="images/image1_thumb.jpg" alt="" />
					
					<section>
						<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris.</p>
					</section>
					
					<footer class="post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a>, <a href="#">CATEGORY 2</a></li>
						</ul>
						
						<a href="single.html" class="more-link">More+</a>
						
					</footer>
					
				</article>
				
			</div>
			
			<!-- BLOG POST 3 (VIDEO) -->
			<div class="article-wrapper type-video">
			
					<article>
					
						<h1>Vimeo Video</h1>
						
						<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti. </p>
						
						<footer class="post-meta">
							
							<ul>
								<li>BY ADMIN</li>
								<li>|</li>
								<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
								<li>|</li>
								<li><a href="#">CATEGORY 2</a></li>
							</ul>							
							
						</footer>
						
					</article>
				
				<iframe src="http://player.vimeo.com/video/7809605?title=0&amp;byline=0&amp;portrait=0" width="764" height="430" frameborder="0"></iframe>
				
			</div>
			
			<!-- BLOG POST 4 (TEXT) -->
			<div class="article-wrapper type-text">
			
				<article>
				
					<h1>Text Only Post</h1>
					
					<section>
						<p>Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est.</p>
					</section>
					
					<footer class="post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 2</a></li>
						</ul>
						
						<a href="single.html" class="more-link">More+</a>
						
					</footer>
					
				</article>				
				
			</div>
			
			<!-- BLOG POST 5 (IMAGE) -->
			<div class="article-wrapper type-image hover">
				
				<article>
				
					<h1><span>Post With Featured Image</span></h1>
					
					<img src="images/image2_thumb.jpg" alt="" />
					
					<section>
						<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris.</p>
					</section>
					
					<footer class="post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a>, <a href="#">CATEGORY 2</a></li>
						</ul>
						
						<a href="single.html" class="more-link">More+</a>
						
					</footer>
					
				</article>
				
			</div>
			
			<!-- BLOG POST 6 (IMAGE) -->
			<div class="article-wrapper type-image hover">
				
				<article>
				
					<h1><span>Post With Featured Image</span></h1>
					
					<img src="images/image3_thumb.jpg" alt="" />
					
					<section>
						<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris.</p>
					</section>
					
					<footer class="post-meta">
						
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a>, <a href="#">CATEGORY 2</a></li>
						</ul>
						
						<a href="single.html" class="more-link">More+</a>
						
					</footer>
					
				</article>
				
			</div>
			
			<!-- BLOG POST 7 (VIDEO) -->
			<div class="article-wrapper type-video">
			
				<article>
					
					<h1>YouTube Video</h1>
						
					<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti. </p>
						
					<footer class="post-meta">
							
						<ul>
							<li>BY ADMIN</li>
							<li>|</li>
							<li><time datetime="009-10-22T13:59:47-04:00">DEC 07 2010</time></li>
							<li>|</li>
							<li><a href="#">CATEGORY 1</a></li>
						</ul>							
							
					</footer>
						
				</article>
				
				<iframe title="YouTube video player" class="youtube-player" type="text/html" width="764" height="430" src="http://www.youtube.com/embed/qZGFjBmD41Q?rel=0&amp;hd=1&amp;wmode=opaque" frameborder="0"></iframe>
				
			</div>
			
		</div>
		
	</div>
	
	<!-- PAGINATION -->
	<div class='pagination'>
		<span class='current'>1</span>
		<a href='#' class='inactive' >2</a>
		<a href='#' class='inactive' >3</a>
		<a href='#'>Next</a>
	</div> 
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>
	
</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/blog.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:03 GMT -->
</html>