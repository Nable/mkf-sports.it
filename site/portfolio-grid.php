<?php include_once('config/menu.php');?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from templates.raw-brand.com/sideways/portfolio-grid.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:04 GMT -->
<head>
	
	<title>MKF Sports</title> 
	<meta charset="utf-8" />
	<meta name="description" content="" > 
	<meta name="keywords" content="" >
	
	<link rel="shortcut icon" href="http://templates.raw-brand.com/favicon.ico" /> 

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- CSS -->
	<link rel="stylesheet" href="style.css" media="all" />
	<link rel="stylesheet" href="css/prettyPhoto_v.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie7.css" media="screen" />
	<![endif]-->
	
	<!-- JAVASCRIPTS -->
	<script src="ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script src="js/raw.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.backstretch.min.js"></script>
	
	<!-- JAVASCRIPT TRIGGERS -->
	<script type="text/javascript">	
		$(document).ready(function(){
			$("a[rel^='prettyPhoto']").prettyPhoto({
				theme: 'dark_square'
			});
		});	
		
		$.backstretch("images/background.jpg", {speed: 'slow'});		
	</script>
	
</head>
<body>

<div id="wrapper">
	
	<!-- SEARCH BAR -->
	<div id="searchbar-holder">
	
		<div id="searchbar">
			
			<ul class="search">
				<li class="widget_search">
					<form method="get" class="searchform" action="./prodotti.php">
						<fieldset>
							<input class="searchsubmit" type="submit" value="Search">
							<input class="text s" type="text" value="" name="s">							
						</fieldset>
					</form>
				</li>
			</ul>
			
			<!-- SOCIAL BUTTONS -->
			<div id="share">
			
				<a href="#" class="share-button"><span>Share</span></a>
				
				<div id="share-box">
					
					<div id="share-holder">
						
						<a href="#" class="email-button">email</a>
						<a href="#" class="rss-button">rss</a>
						<a href="#" class="facebook-button">Facebook</a>
						<a href="#" class="twitter-button">twitter</a>
						<a href="#" class="digg-button">digg</a>
						<a href="#" class="myspace-button">myspace</a>
						<a href="#" class="dribble-button">dribble</a>
						<a href="#" class="flickr-button">flickr</a>
						<a href="#" class="linkedin-button">linkedin</a>
						<a href="#" class="vimeo-button">vimeo</a>
						<a href="#" class="youtube-button">youtube</a>
						
					</div>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<div id="sidebar">
		
		<!-- LOGO -->
		<header>
		
			<img src="images/logo.png" alt="Website Logo" />
		
			<h1></h1>
			<h2></h2>
		
		</header>
		
		<!-- NAVIGATION -->
		<?php getNavigationMenu();?>
	
	</div>

	<div id="content" class="clearfix">
		
		<div class="article-wrapper">
			
			<!-- CONTENT -->
			<article>
			
				<h1>Grid Portfolio</h1>
				
				<p>Sed vel tristique urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam at justo purus, sed tristique mauris. Proin mollis est ac magna pretium laoreet. Suspendisse potenti. Phasellus felis ipsum, convallis at lobortis in, volutpat in libero. Donec nulla orci, vestibulum sodales sagittis at, euismod quis est. Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus. Nulla in egestas mauris. In a consectetur dui. Fusce mi felis, tincidunt a molestie sed, hendrerit in massa. Fusce justo lacus, cursus ac convallis auctor, imperdiet ut mi. Sed vitae urna erat. In neque eros, imperdiet eget euismod in, vulputate non neque.</p>
				
				<!-- PORTFOLIO FILTER -->
				<ul id="filter">
					<li><h4>Filter:</h4></li>
					<li class='current'><a href='#' title="all">All</a></li>
					<li><a href='#' title="print">Print</a></li>
					<li><a href='#' title="web-design">Web Design</a></li>
					<li><a href='#' title="video">Video</a></li>
				</ul>
		
				<ul id="portfolio" class="clearfix">
					
					<li class="print">
					
						<img src="images/portoflio_image1.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="web-design print">
					
						<img src="images/portoflio_image2.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="video">
					
						<img src="images/portoflio_image3.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="web-design">
					
						<img src="images/portoflio_image4.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="video web-design">
					
						<img src="images/portoflio_image5.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="print">
					
						<img src="images/portoflio_image6.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="video">
					
						<img src="images/portoflio_image1.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="print">
					
						<img src="images/portoflio_image2.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="print video">
					
						<img src="images/portoflio_image3.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="web-design">
					
						<img src="images/portoflio_image4.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="print">
					
						<img src="images/portoflio_image5.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
					
					<li class="print video web-design">
					
						<img src="images/portoflio_image6.jpg" alt="" />
						
						<div class="info">
							
							<h1>Item Title</h1>
							
							<p>Nulla nibh massa, convallis non lobortis in, tincidunt eu mi. Nunc interdum mauris a tortor imperdiet rhoncus.</p>
							
							<a href="images/image1.jpg" rel="prettyphoto" class="more-link">More+</a>
							
						</div>
					
					</li>
				
				</ul>
		
			</article>
		
		</div>
		
	</div>
	
	<div id="push"></div>
	
</div>

<!-- FOOTER -->
<footer>
	
	<nav>
		
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Prodotti</a></li>
			<li><a href="#">Profilo</a></li>
			<li><a href="#">Tecnologia</a></li>
			<li><a href="#">Sede</a></li>
			<li><a href="#">Taglie</a></li>
			<li><a href="#">Abbigliamento</a></li>	
			<li><a href="#">Contatti</a></li>			
		</ul>
		
	</nav>
	
	<p>&#169; 2010 Sideways. All rights reserved.</p>

</footer>

</body>

<!-- Mirrored from templates.raw-brand.com/sideways/portfolio-grid.html by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 28 Dec 2010 04:44:35 GMT -->
</html>